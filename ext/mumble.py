
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from sys import stderr
try:
	from pyaudio import PyAudio, paInt16
	from pymumble.constants import PYMUMBLE_CLBK_CONNECTED, PYMUMBLE_CLBK_DISCONNECTED,  \
		PYMUMBLE_CLBK_CHANNELCREATED, PYMUMBLE_CLBK_USERCREATED, PYMUMBLE_CLBK_USERREMOVED, \
		PYMUMBLE_CLBK_TEXTMESSAGERECEIVED, PYMUMBLE_CLBK_SOUNDRECEIVED
	from pymumble.messages import TextPrivateMessage
	from pymumble.mumble import Mumble
	mumble_available = True
except ImportError:
	mumble_available = False

from PyQt5.QtCore import QThread

from base.phone import RMS_amplitude, AbstractLandLineManager

from session.config import settings, version_string


# ---------- Constants ----------

mumble_nickname_prefix = 'ATC-pie_' # exceed 7 characters in length, not to conflict with other FGMS callsigns
landline_request_text_msg = '___ATC-pie___ LL_REQUEST'
landline_drop_text_msg = '___ATC-pie___ LL_DROP'

audio_chunk_size = 1024
audio_sample_rate = 48000  # doc says "pymumble soundchunk.pcm is 48 kHz and 16 bits" (cf. format=paInt16 for latter)

# -------------------------------


def nickname_from_callsign(callsign):
	return mumble_nickname_prefix + callsign

def callsign_from_nickname(nickname):
	if len(nickname) > len(mumble_nickname_prefix) and nickname.startswith(mumble_nickname_prefix):
		return nickname[len(mumble_nickname_prefix):]
	else:
		raise ValueError('Not a recognised phone line callsign: ' + nickname)




class MumbleCommunicator(AbstractLandLineManager, QThread):
	def __init__(self, parent):
		AbstractLandLineManager.__init__(self)
		QThread.__init__(self, parent)
		self.my_nickname = nickname_from_callsign(settings.my_callsign)
		self.ll_sessions = {} # callsign -> Mumble user session ID
		self.bot = Mumble(settings.land_line_server_host, self.my_nickname, port=settings.land_line_server_port)
		self.bot.set_application_string('ATC-pie %s' % version_string)
		self.bot.set_receive_sound(True)
		self.connecting = False
		self.running = False
		self.move_to_channel_id = None # will be known at connection time
		self.call_in_progress = None # user SID of current call
	
	def run(self):
		self.bot.callbacks.add_callback(PYMUMBLE_CLBK_CONNECTED, self.mumbleConnected)
		self.bot.callbacks.add_callback(PYMUMBLE_CLBK_CHANNELCREATED, self.channelCreated)
		self.bot.callbacks.add_callback(PYMUMBLE_CLBK_DISCONNECTED, self.mumbleDisconnected)
		self.bot.callbacks.add_callback(PYMUMBLE_CLBK_USERCREATED, self.userConnects)
		self.bot.callbacks.add_callback(PYMUMBLE_CLBK_USERREMOVED, self.userDisconnects)
		self.bot.callbacks.add_callback(PYMUMBLE_CLBK_TEXTMESSAGERECEIVED, self.receiveTextMessage)
		self.connecting = True
		self.running = True
		self.move_to_channel_id = None
		self.call_in_progress = None
		self.bot.start() # eventually triggers "mumbleConnected" callback, or thread exits
		while self.connecting and self.bot.is_alive():
			self.msleep(100)
		if not self.bot.is_alive(): # e.g. Mumble server host not found
			print('Could not connect to Mumble server; phone lines disabled.', file=stderr) # no user ever created
			return # finishes thread
		pa = PyAudio()
		self.audio_in = pa.open(format=paInt16, channels=1, rate=audio_sample_rate,
				frames_per_buffer=audio_chunk_size, input=True, start=False)
		self.audio_out = pa.open(format=paInt16, channels=1, rate=audio_sample_rate,
				frames_per_buffer=audio_chunk_size, output=True)
		self.bot.callbacks.add_callback(PYMUMBLE_CLBK_SOUNDRECEIVED, self.soundReceived) # keep after "audio_out" creation
		self.bot.is_ready() # keep this for when we know we are connected (bot deadlock otherwise)
		self.bot.users.myself.comment('ATC-pie phone line connection, ignoring all foreign input.')
		self.bot.users.myself.mute()
		self.bot.users.myself.deafen()
		while self.running:
			if self.call_in_progress is None:
				if not self.audio_in.is_stopped():
					self.audio_in.stop_stream()
				self.msleep(100)
			else:
				if self.audio_in.is_stopped():
					self.audio_in.start_stream()
				data = self.audio_in.read(audio_chunk_size, exception_on_overflow=False)
				if RMS_amplitude(data) > settings.phone_line_squelch:
					self.bot.sound_output.add_sound(data)
		self.bot.stop()
		self.bot.join(timeout=2) # timeout in seconds (float)
		self.audio_in.close()
		self.audio_out.close()
		pa.terminate()
	
	def mumbleDisconnected(self):
		if self.running: # unexpected disconnection
			print('Phone line system disconnected.', file=stderr)
			self.running = False # to quit
			for callsign in self.ll_sessions:
				self.destroyLandLine(callsign)
	
	def stopAndWait(self):
		self.running = False
		self.wait()
	
	def _sendTextLineToCallsign(self, callsign, text_line):
		try:
			self.bot.execute_command(TextPrivateMessage(self.ll_sessions[callsign], text_line))
		except KeyError:
			print('MumbleCommunicator could not send text line to callsign:', callsign, file=stderr)
	
	
	## CALLBACKS
	
	def mumbleConnected(self):
		print('Phone line system connected.')
		if self.move_to_channel_id is not None:
			self.bot.users.myself.move_in(self.move_to_channel_id)
		elif settings.land_line_move_to_channel != '':
			print('No such channel on server:', settings.land_line_move_to_channel, file=stderr)
		self.connecting = False
	
	def channelCreated(self, channel):
		if settings.land_line_move_to_channel != '' and channel['name'] == settings.land_line_move_to_channel:
			self.move_to_channel_id = channel['channel_id'] # their fault if multiple channels with this name in the tree
	
	def userConnects(self, user):
		if user['name'] != self.my_nickname:
			try:
				callsign = callsign_from_nickname(user['name'])
				self.ll_sessions[callsign] = user['session']
				self.createLandLine(callsign)
			except ValueError: # not a recognised ATC-pie phone line nickname
				pass
	
	def userDisconnects(self, user, message):
		try:
			callsign = next(cs for cs, sid in self.ll_sessions.items() if sid == user['session'])
			del self.ll_sessions[callsign]
			self.destroyLandLine(callsign)
		except StopIteration: # not a known ATC-pie phone line
			pass
	
	def soundReceived(self, user, soundchunk):
		if user['session'] == self.call_in_progress:
			self.audio_out.write(soundchunk.pcm)
	
	def receiveTextMessage(self, txtmsg):
		try:
			callsign = next(cs for cs, sid in self.ll_sessions.items() if sid == txtmsg.actor)
			if txtmsg.message == landline_request_text_msg:
				self.receiveLandLineRequest(callsign)
			elif txtmsg.message == landline_drop_text_msg:
				self.receiveLandLineDrop(callsign)
			else:
				print('Ignored Mumble text message from %s:' % callsign, txtmsg.message, file=stderr)
		except StopIteration: # not received from a known ATC-pie phone line
			print('Ignored Mumble text message from unknown sender %s:' % txtmsg.actor, txtmsg.message, file=stderr)
	
	
	## Defining AbstractLandLineManager methods below
	def startVoiceWith(self, atc):
		try:
			sid = self.ll_sessions[atc]
			self.bot.sound_output.set_whisper(sid)
			self.bot.users.myself.unmute()
			self.bot.users.myself.undeafen()
			self.call_in_progress = sid
		except KeyError:
			print('KeyError in MumbleCommunicator.startVoiceWith:', atc, file=stderr)
	
	def stopVoice(self):
		self.call_in_progress = None
		self.bot.users.myself.mute()
		self.bot.users.myself.deafen()
		self.bot.sound_output.remove_whisper()
	
	def sendLandLineRequest(self, atc):
		self._sendTextLineToCallsign(atc, landline_request_text_msg)
	
	def sendLandLineDrop(self, atc):
		self._sendTextLineToCallsign(atc, landline_drop_text_msg)
