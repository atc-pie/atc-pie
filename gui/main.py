
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from sys import stderr
from datetime import timedelta

from PyQt5.QtCore import Qt, QUrl, QTimer
from PyQt5.QtGui import QDesktopServices, QIcon
from PyQt5.QtWidgets import QMainWindow, QInputDialog, QFileDialog, QMessageBox, QAction, QActionGroup, QLabel

from ui.mainWindow import Ui_mainWindow

from base.coords import EarthCoords
from base.fpl import FPL
from base.utc import now, timestr
from base.weather import hPa2inHg, mkWeather
from base.nav import world_routing_db
from base.strip import soft_link_detail, departure_clearance_detail

from ext.data import read_bg_img, read_route_presets, import_entry_exit_data, make_FGFS_models_liveries
from ext.fgfs import FlightGearTowerViewer
from ext.sct import extract_sector
from ext.sr import speech_recognition_available, prepare_SR_language_files, cleanup_SR_language_files

from gui.actions import new_strip_dialog, edit_strip, receive_strip, recover_strip, strip_auto_print_check, kill_aircraft, \
		receive_CPDLC_transfer_request, receive_CPDLC_transfer_response
from gui.dialog.adSurfaces import AdSfcUseDialog
from gui.dialog.depClearance import DepartureClearanceEditDialog, DepartureClearanceViewDialog
from gui.dialog.locInfo import LocationInfoDialog
from gui.dialog.miscDialogs import AboutDialog, DiscardedStripsDialog, RadarMeasurementLog
from gui.dialog.settings import LocalSettingsDialog, GeneralSettingsDialog, SystemSettingsDialog, SoloSessionSettingsDialog, AdditionalViewersDialog
from gui.dialog.startSession import StartSoloDialog_AD, StartFgSessionDialog, StartFsdDialog, StartStudentSessionDialog, StartTeacherSessionDialog
from gui.dialog.statistics import StatisticsDialog
from gui.misc import IconFile, signals, selection, RadioKeyEventFilter
from gui.panels.cpdlcPanels import CpdlcPanel
from gui.panels.radarScope import ScopeFrame
from gui.panels.radioCentre import AtisDialog
from gui.panels.selectionInfo import SelectionInfoToolbarWidget
from gui.panels.stripPanes import LooseStripPane, StripRacksPane
from gui.panels.teachingConsole import TeachingConsole
from gui.panels.unitConv import UnitConversionWindow
from gui.widgets.miscWidgets import QuickReference
from gui.widgets.adWidgets import WorldAirportNavigator
from gui.widgets.basicWidgets import flash_widget, Ticker, AlarmClockButton

from session.config import settings, radar_save_state_keyword, loosebay_save_state_keyword, stripracks_save_state_keyword
from session.env import env, default_tower_height
from session.manager import SessionManager, SessionType, student_callsign, teacher_callsign
from session.managers.flightGearMP import FlightGearSessionManager
from session.managers.fsdMP import FsdSessionManager
from session.managers.teacher import TeacherSessionManager
from session.managers.student import StudentSessionManager
from session.managers.solo import SoloSessionManager_AD, SoloSessionManager_CTR
from session.models.liveStrips import default_rack_name
from session.models.discardedStrips import ShelfFilterModel


# ---------- Constants ----------

stylesheet_file = 'CONFIG/main-stylesheet.qss'
dock_layout_file = 'CONFIG/dock-layout.bin'

subsecond_tick_interval = 300 # ms
subminute_tick_interval = 10 * 1000 # ms
status_bar_message_timeout = 5000 # ms
session_start_temp_lock_duration = 5000 # ms
forward_time_skip = timedelta(seconds=10)

dock_flash_stylesheet = 'QDockWidget::title { background: yellow }'

OSM_zoom_level = 7
OSM_base_URL_fmt = 'http://www.openstreetmap.org/#map=%d/%f/%f'

airport_gateway_URL = 'http://gateway.x-plane.com/airports/page'
lenny64_newEvent_URL = 'http://flightgear-atc.alwaysdata.net/new_event.php'
video_tutorial_URL = 'http://www.youtube.com/playlist?list=PL1EQKKHhDVJvvWpcX_BqeOIsmeW2A_8Yb'
FAQ_URL = 'http://wiki.flightgear.org/ATC-pie_FAQ'

# -------------------------------

user_panels_save_state_kwd = {
	radar_save_state_keyword: ScopeFrame,
	loosebay_save_state_keyword: LooseStripPane,
	stripracks_save_state_keyword: StripRacksPane
}

def stateSaveKwd(panel):
	return next(kwd for kwd, t in user_panels_save_state_kwd.items() if isinstance(panel, t))


def mk_OSM_URL(coords):
	return OSM_base_URL_fmt % (OSM_zoom_level, coords.lat, coords.lon)


def setDockAndActionIcon(icon_file, action, dock):
	icon = QIcon(icon_file)
	action.setIcon(icon)
	dock.setWindowIcon(icon)
	
def raise_dock(dock):
	dock.show()
	dock.raise_()
	dock.widget().setFocus()
	flash_widget(dock, dock_flash_stylesheet)

def open_raise_window(window):
	window.show()
	window.raise_()



class MainWindow(QMainWindow, Ui_mainWindow):
	def __init__(self, launcher, parent=None):
		QMainWindow.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setAttribute(Qt.WA_DeleteOnClose)
		self.launcher = launcher
		settings.controlled_tower_viewer = FlightGearTowerViewer(self)
		settings.session_manager = SessionManager(self, None) # Dummy manager
		self.updateWindowTitle()
		self.user_panels = []
		self.last_RDF_qdm = None
		self.session_start_temp_lock_timer = QTimer(self)
		self.session_start_temp_lock_timer.setSingleShot(True)
		self.session_start_temp_lock_timer.timeout.connect(self.releaseSessionStartTempLock)
		
		## Restore saved dock layout
		try:
			with open(dock_layout_file, 'rb') as f: # Restore saved dock arrangement
				self.restoreState(f.read())
		except FileNotFoundError: # Fall back on default dock arrangement
			# left docks, top zone: contact details (hidden), notepads (hidden), TWR ctrl (hidden), navigator, FPLs, weather (on top)
			self.tabifyDockWidget(self.selection_info_dock, self.notepads_dock)
			self.tabifyDockWidget(self.selection_info_dock, self.towerView_dock)
			self.tabifyDockWidget(self.selection_info_dock, self.navigator_dock)
			self.tabifyDockWidget(self.selection_info_dock, self.FPL_dock)
			self.tabifyDockWidget(self.selection_info_dock, self.weather_dock)
			self.selection_info_dock.hide()
			self.notepads_dock.hide()
			self.towerView_dock.hide()
			# left docks, bottom zone: instructions (hidden), radios (hidden), notifications (on top)
			self.tabifyDockWidget(self.instructions_dock, self.radio_dock)
			self.tabifyDockWidget(self.instructions_dock, self.notification_dock)
			self.instructions_dock.hide()
			self.radio_dock.hide()
			# right docks: RWY boxes (hidden), strips, ATC coord.
			self.rwyBoxes_dock.hide()
			# bottom docks (all hidden): radio chat, ATC chat
			self.radioTextChat_dock.hide()
			self.atcTextChat_dock.hide()
			# toolbars, in order: general, strip/FPL actions (hidden), contact info, radar assistance (hidden), docks (right-most)
			self.insertToolBar(self.docks_toolbar, self.general_toolbar) # = "insert before"
			self.insertToolBar(self.docks_toolbar, self.stripFplActions_toolbar)
			self.insertToolBar(self.docks_toolbar, self.selectionInfo_toolbar)
			self.radarAssistance_toolbar.hide()
			self.stripFplActions_toolbar.hide()
			self.insertToolBar(self.docks_toolbar, self.radarAssistance_toolbar)
		
		## Permanent tool/status bar widgets
		self.selectionInfo_toolbarWidget = SelectionInfoToolbarWidget(self)
		self.selectionInfo_toolbar.addWidget(self.selectionInfo_toolbarWidget)
		self.METAR_statusBarLabel = QLabel()
		self.PTT_statusBarLabel = QLabel()
		self.PTT_statusBarLabel.setToolTip('Keyboard PTT')
		self.RDF_statusBarLabel = QLabel()
		self.RDF_statusBarLabel.setToolTip('Current signal / last QDM')
		self.wind_statusBarLabel = QLabel()
		self.QNH_statusBarLabel = QLabel()
		self.QNH_statusBarLabel.setToolTip('hPa / inHg')
		self.clock_statusBarLabel = QLabel()
		self.alarmClock_statusBarButtons = [AlarmClockButton('1', self), AlarmClockButton('2', self)]
		self.statusbar.addWidget(self.METAR_statusBarLabel)
		self.statusbar.addPermanentWidget(self.PTT_statusBarLabel)
		self.statusbar.addPermanentWidget(self.RDF_statusBarLabel)
		self.statusbar.addPermanentWidget(self.wind_statusBarLabel)
		self.statusbar.addPermanentWidget(self.QNH_statusBarLabel)
		for button in self.alarmClock_statusBarButtons:
			self.statusbar.addPermanentWidget(button)
			button.alarm.connect(lambda lbl=button.label(): self.notification_pane.notifyAlarmClockTimedOut('Alarm clock %s timed out' % lbl))
		self.statusbar.addPermanentWidget(self.clock_statusBarLabel)

		## Memory-persistent windows and dialogs
		self.start_solo_AD_dialog = StartSoloDialog_AD(self)
		self.FG_connect_dialog = StartFgSessionDialog(self)
		self.FSD_connect_dialog = StartFsdDialog(self)
		self.start_student_session_dialog = StartStudentSessionDialog(self)
		self.start_teacher_session_dialog = StartTeacherSessionDialog(self)
		self.recall_cheat_dialog = DiscardedStripsDialog(self, ShelfFilterModel(self, env.discarded_strips, False), 'Sent and deleted strips')
		self.shelf_dialog = DiscardedStripsDialog(self, ShelfFilterModel(self, env.discarded_strips, True), 'Strip shelf')
		self.location_info_dialog = LocationInfoDialog(self)
		self.statistics_dialog = StatisticsDialog(self)
		self.about_dialog = AboutDialog(self)
		self.CPDLC_panel = CpdlcPanel()
		self.teaching_console = TeachingConsole()
		self.unit_converter = UnitConversionWindow(parent=self)
		self.world_airport_navigator = WorldAirportNavigator(parent=self)
		self.quick_reference = QuickReference(parent=self)
		self.DEP_clearance_view = DepartureClearanceViewDialog(parent=self)
		self.radar_measurement_log = RadarMeasurementLog(parent=self, visibilityAction=self.measuringLogsCoordinates_system_action)
		
		# Populate menus (toolbar visibility, central panel selection, airport viewpoints)
		self.general_viewToolbar_action = self.general_toolbar.toggleViewAction()
		self.stripActions_viewToolbar_action = self.stripFplActions_toolbar.toggleViewAction()
		self.docks_viewToolbar_action = self.docks_toolbar.toggleViewAction()
		self.selectionInfo_viewToolbar_action = self.selectionInfo_toolbar.toggleViewAction()
		self.radarAssistance_viewToolbar_action = self.radarAssistance_toolbar.toggleViewAction()
		self.view_toolbars_menu.addAction(self.general_viewToolbar_action)
		self.view_toolbars_menu.addAction(self.stripActions_viewToolbar_action)
		self.view_toolbars_menu.addAction(self.docks_viewToolbar_action)
		self.view_toolbars_menu.addAction(self.selectionInfo_viewToolbar_action)
		self.view_toolbars_menu.addAction(self.radarAssistance_viewToolbar_action)
		
		if env.airport_data is None or len(env.airport_data.viewpoints) == 0:
			self.system_activeViewpoint_menu.setEnabled(False)
		else:
			viewPointSelection_actionGroup = QActionGroup(self)
			for vp_i, (vp_pos, vp_h, vp_name) in enumerate(env.airport_data.viewpoints):
				action = QAction('%s (%d ft ASFC)' % (vp_name, vp_h + .5), self)
				action.setCheckable(True)
				action.triggered.connect(lambda ignore_checked, i=vp_i: self.selectIndicateViewpoint(i))
				viewPointSelection_actionGroup.addAction(action)
			actions = viewPointSelection_actionGroup.actions()
			self.system_activeViewpoint_menu.addActions(actions)
			try:
				actions[settings.selected_viewpoint].setChecked(True)
			except IndexError:
				actions[0].setChecked(True)

		self.centralPanelSelection_actionGroup = QActionGroup(self)
		self.centralPanel_selectNone_action = QAction('None', self)
		self.centralPanel_selectNone_action.setCheckable(True)
		self.centralPanel_selectNone_action.setChecked(True)
		self.centralPanel_selectNone_action.triggered.connect(lambda: self.central_workspace.setCurrentPanel(None))
		self.centralPanelSelection_actionGroup.addAction(self.centralPanel_selectNone_action)
		self.view_centralPanel_menu.addAction(self.centralPanel_selectNone_action)
		self.view_centralPanel_menu.addSeparator()
		self._mkCentralPanelAction(self.CPDLC_panel, False)
		self._mkCentralPanelAction(self.teaching_console, False)
		
		# Add the actions not used in the main window, or only in removable toolbars
		for action in self.startTimer1_action, self.forceStartTimer1_action, self.startTimer2_action, self.forceStartTimer2_action, \
				self.newStrip_action, self.newLinkedStrip_action, self.newFPL_action, self.newLinkedFPL_action, \
				self.flagUnflagSelection_action, self.toggleMachNumbers_action:
			self.addAction(action)
		
		# Populate icons
		self.newStrip_action.setIcon(QIcon(IconFile.action_newStrip))
		self.newLinkedStrip_action.setIcon(QIcon(IconFile.action_newLinkedStrip))
		self.newFPL_action.setIcon(QIcon(IconFile.action_newFPL))
		self.newLinkedFPL_action.setIcon(QIcon(IconFile.action_newLinkedFPL))
		self.teachingConsole_view_action.setIcon(QIcon(IconFile.panel_teaching))
		self.unitConversionTool_view_action.setIcon(QIcon(IconFile.panel_unitConv))
		self.worldAirportNavigator_view_action.setIcon(QIcon(IconFile.panel_airportList))
		self.locationInfo_view_action.setIcon(QIcon(IconFile.panel_locInfo))
		self.generalSettings_options_action.setIcon(QIcon(IconFile.action_generalSettings))
		self.soloSessionSettings_system_action.setIcon(QIcon(IconFile.action_sessionSettings))
		self.adSurfacesUse_options_action.setIcon(QIcon(IconFile.action_adSfcUse))
		self.newLooseStripBay_view_action.setIcon(QIcon(IconFile.action_newLooseStripBay))
		self.newRadarScreen_view_action.setIcon(QIcon(IconFile.action_newRadarScreen))
		self.newStripRackPanel_view_action.setIcon(QIcon(IconFile.action_newRackPanel))
		self.cpdlcPanel_view_action.setIcon(QIcon(IconFile.panel_CPDLC))
		self.primaryRadar_options_action.setIcon(QIcon(IconFile.option_primaryRadar))
		self.approachSpacingHints_options_action.setIcon(QIcon(IconFile.option_approachSpacingHints))
		self.runwayOccupationWarnings_options_action.setIcon(QIcon(IconFile.option_runwayOccupationMonitor))
		self.routeConflictWarnings_options_action.setIcon(QIcon(IconFile.option_routeConflictWarnings))
		self.trafficIdentification_options_action.setIcon(QIcon(IconFile.option_identificationAssistant))
		
		setDockAndActionIcon(IconFile.panel_ATCs, self.atcCoordination_dockView_action, self.atcCoordination_dock)
		setDockAndActionIcon(IconFile.panel_atcChat, self.atcTextChat_dockView_action, self.atcTextChat_dock)
		setDockAndActionIcon(IconFile.panel_FPLs, self.FPLs_dockView_action, self.FPL_dock)
		setDockAndActionIcon(IconFile.panel_instructions, self.instructions_dockView_action, self.instructions_dock)
		setDockAndActionIcon(IconFile.panel_navigator, self.navpoints_dockView_action, self.navigator_dock)
		setDockAndActionIcon(IconFile.panel_notepads, self.notepads_dockView_action, self.notepads_dock)
		setDockAndActionIcon(IconFile.panel_notifications, self.notificationArea_dockView_action, self.notification_dock)
		setDockAndActionIcon(IconFile.panel_radios, self.fgcom_dockView_action, self.radio_dock)
		setDockAndActionIcon(IconFile.panel_runwayBox, self.runwayBoxes_dockView_action, self.rwyBoxes_dock)
		setDockAndActionIcon(IconFile.panel_selInfo, self.radarContactDetails_dockView_action, self.selection_info_dock)
		setDockAndActionIcon(IconFile.panel_racks, self.strips_dockView_action, self.strip_dock)
		setDockAndActionIcon(IconFile.panel_txtChat, self.radioTextChat_dockView_action, self.radioTextChat_dock)
		setDockAndActionIcon(IconFile.panel_twrView, self.towerView_dockView_action, self.towerView_dock)
		setDockAndActionIcon(IconFile.panel_weather, self.weather_dockView_action, self.weather_dock)
		
		# TICKED STATES set before connections
		self.muteNotifications_options_action.setChecked(settings.mute_notifications)
		self.primaryRadar_options_action.setChecked(settings.primary_radar_active)
		self.routeConflictWarnings_options_action.setChecked(settings.route_conflict_warnings)
		self.trafficIdentification_options_action.setChecked(settings.traffic_identification_assistant)
		self.runwayOccupationWarnings_options_action.setChecked(settings.monitor_runway_occupation)
		self.approachSpacingHints_options_action.setChecked(settings.APP_spacing_hints)
		
		# action CONNECTIONS
		# non-menu actions
		self.newStrip_action.triggered.connect(lambda: new_strip_dialog(self, default_rack_name, linkToSelection=False))
		self.newLinkedStrip_action.triggered.connect(lambda: new_strip_dialog(self, default_rack_name, linkToSelection=True))
		self.newFPL_action.triggered.connect(lambda: self.FPL_panel.createLocalFPL(link=None))
		self.newLinkedFPL_action.triggered.connect(lambda: self.FPL_panel.createLocalFPL(link=selection.strip))
		self.flagUnflagSelection_action.triggered.connect(self.flagUnflagSelection)
		self.toggleMachNumbers_action.triggered.connect(signals.toggleMachNumbers.emit)
		self.startTimer1_action.triggered.connect(lambda: self.startGuiTimer(0, False))
		self.forceStartTimer1_action.triggered.connect(lambda: self.startGuiTimer(0, True))
		self.startTimer2_action.triggered.connect(lambda: self.startGuiTimer(1, False))
		self.forceStartTimer2_action.triggered.connect(lambda: self.startGuiTimer(1, True))
		# system menu
		self.soloSession_system_action.triggered.connect(lambda: self.startStopSession(self.start_solo))
		self.flightGearSession_system_action.triggered.connect(lambda: self.startStopSession(self.start_FlightGearSession))
		self.fsdConnection_system_action.triggered.connect(lambda: self.startStopSession(self.start_FSD))
		self.teacherSession_system_action.triggered.connect(lambda: self.startStopSession(self.start_teaching))
		self.studentSession_system_action.triggered.connect(lambda: self.startStopSession(self.start_learning))
		self.towerView_system_action.triggered.connect(self.toggleTowerWindow)
		self.additionalViewers_system_action.triggered.connect(self.configureAdditionalViewers)
		self.reloadBgImages_system_action.triggered.connect(self.reloadBackgroundImages)
		self.reloadFgAcftModels_system_action.triggered.connect(self.reloadFgAcftModels)
		self.reloadRoutePresetsAndEntryExitPoints_system_action.triggered.connect(self.reloadRoutePresetsAndEntryExitPoints)
		self.reloadStylesheetAndColours_system_action.triggered.connect(self.reloadStylesheetAndColours)
		self.airportGateway_system_action.triggered.connect(lambda: self.goToURL(airport_gateway_URL))
		self.openStreetMap_system_action.triggered.connect(lambda: self.goToURL(mk_OSM_URL(env.radarPos())))
		self.announceFgSession_system_action.triggered.connect(lambda: self.goToURL(lenny64_newEvent_URL))
		self.measuringLogsCoordinates_system_action.toggled.connect(self.switchMeasuringCoordsLog)
		self.extractSectorFile_system_action.triggered.connect(self.extractSectorFile)
		self.repositionBgImages_system_action.triggered.connect(self.repositionRadarBgImages)
		self.phoneSquelchEdit_system_action.toggled.connect(self.atcCoordination_pane.phoneSquelch_widget.setVisible)
		self.soloSessionSettings_system_action.triggered.connect(self.openSoloSessionSettings)
		self.locationSettings_system_action.triggered.connect(self.openLocalSettings)
		self.systemSettings_system_action.triggered.connect(self.openSystemSettings)
		self.changeLocation_system_action.triggered.connect(self.changeLocation)
		self.quit_system_action.triggered.connect(self.close)
		# view menu
		self.recallWindowState_view_action.triggered.connect(self.recallWindowState)
		self.saveDockLayout_view_action.triggered.connect(self.saveDockLayout)
		self.atcCoordination_dockView_action.triggered.connect(lambda: raise_dock(self.atcCoordination_dock))
		self.atcTextChat_dockView_action.triggered.connect(lambda: raise_dock(self.atcTextChat_dock))
		self.FPLs_dockView_action.triggered.connect(lambda: raise_dock(self.FPL_dock))
		self.instructions_dockView_action.triggered.connect(lambda: raise_dock(self.instructions_dock))
		self.navpoints_dockView_action.triggered.connect(lambda: raise_dock(self.navigator_dock))
		self.notepads_dockView_action.triggered.connect(lambda: raise_dock(self.notepads_dock))
		self.notificationArea_dockView_action.triggered.connect(lambda: raise_dock(self.notification_dock))
		self.fgcom_dockView_action.triggered.connect(lambda: raise_dock(self.radio_dock))
		self.runwayBoxes_dockView_action.triggered.connect(lambda: raise_dock(self.rwyBoxes_dock))
		self.radarContactDetails_dockView_action.triggered.connect(lambda: raise_dock(self.selection_info_dock))
		self.strips_dockView_action.triggered.connect(lambda: raise_dock(self.strip_dock))
		self.radioTextChat_dockView_action.triggered.connect(lambda: raise_dock(self.radioTextChat_dock))
		self.towerView_dockView_action.triggered.connect(lambda: raise_dock(self.towerView_dock))
		self.weather_dockView_action.triggered.connect(lambda: raise_dock(self.weather_dock))
		self.closeNonDockableWindows_view_action.triggered.connect(signals.closeNonDockableWindows.emit)
		self.newLooseStripBay_view_action.triggered.connect(lambda: self.newUserPanel(LooseStripPane()))
		self.newRadarScreen_view_action.triggered.connect(lambda: self.newUserPanel(ScopeFrame()))
		self.newStripRackPanel_view_action.triggered.connect(lambda: self.newUserPanel(StripRacksPane()))
		self.cpdlcPanel_view_action.triggered.connect(lambda: self.showRaisePanel(self.CPDLC_panel))
		self.teachingConsole_view_action.triggered.connect(lambda: self.showRaisePanel(self.teaching_console))
		self.selectedStrip_view_action.triggered.connect(self.openSelectedStrip)
		self.latestCpdlcDialogue_action.triggered.connect(self.showLastCpdlcDialogueForSelection)
		self.depClearance_view_action.triggered.connect(self.showDepClearanceForSelection)
		self.atis_view_action.triggered.connect(self.openAtisDialog) # TODO icon for toolbar
		self.locationInfo_view_action.triggered.connect(self.location_info_dialog.exec)
		self.statistics_view_action.triggered.connect(self.statistics_dialog.exec)
		self.unitConversionTool_view_action.triggered.connect(lambda: open_raise_window(self.unit_converter))
		self.worldAirportNavigator_view_action.triggered.connect(lambda: open_raise_window(self.world_airport_navigator))
		# options menu
		self.muteNotifications_options_action.toggled.connect(self.muteNotificationSounds)
		self.primaryRadar_options_action.toggled.connect(self.switchPrimaryRadar)
		self.routeConflictWarnings_options_action.toggled.connect(self.switchConflictWarnings)
		self.trafficIdentification_options_action.toggled.connect(self.switchTrafficIdentification)
		self.runwayOccupationWarnings_options_action.toggled.connect(self.switchRwyOccupationIndications)
		self.approachSpacingHints_options_action.toggled.connect(self.switchApproachSpacingHints)
		self.adSurfacesUse_options_action.triggered.connect(self.configureAdSfcUse)
		self.generalSettings_options_action.triggered.connect(self.openGeneralSettings)
		# cheat menu
		self.pauseSimulation_cheat_action.toggled.connect(self.pauseSession)
		self.skipTimeForward_cheat_action.triggered.connect(self.skipTimeForwardOnce)
		self.spawnAircraft_cheat_action.triggered.connect(self.spawnAircraft)
		self.killSelectedAircraft_cheat_action.triggered.connect(self.killSelectedAircraft)
		self.popUpMsgOnRejectedInstr_cheat_action.toggled.connect(self.setRejectedInstrPopUp)
		self.showRecognisedVoiceStrings_cheat_action.toggled.connect(self.setShowRecognisedVoiceStrings)
		self.ensureClearWeather_cheat_action.toggled.connect(self.ensureClearWeather)
		self.ensureDayLight_cheat_action.triggered.connect(settings.controlled_tower_viewer.ensureDayLight)
		self.changeTowerHeight_cheat_action.triggered.connect(self.changeTowerHeight)
		self.radarCheatMode_cheat_action.toggled.connect(self.setRadarCheatMode)
		self.showAcftCheatToggles_cheat_action.toggled.connect(self.showAcftCheatToggles)
		self.recallDiscardedStrip_cheat_action.triggered.connect(self.recall_cheat_dialog.exec)
		# help menu
		self.quickReference_help_action.triggered.connect(lambda: open_raise_window(self.quick_reference))
		self.videoTutorial_help_action.triggered.connect(lambda: self.goToURL(video_tutorial_URL))
		self.FAQ_help_action.triggered.connect(lambda: self.goToURL(FAQ_URL))
		self.about_help_action.triggered.connect(self.about_dialog.exec)

		## More signal connections
		env.radar.lostContact.connect(self.aircraftHasDisappeared)
		env.strips.rwyBoxFreed.connect(lambda box, strip: env.airport_data.resetRwySepTimer(box, strip.lookup(FPL.WTC)))
		signals.openShelfRequest.connect(self.shelf_dialog.exec)
		signals.privateAtcChatRequest.connect(lambda: raise_dock(self.atcTextChat_dock))
		signals.weatherDockRaiseRequest.connect(lambda: raise_dock(self.weather_dock))
		signals.atisDialogRequest.connect(self.openAtisDialog)
		signals.stripRecall.connect(recover_strip)
		signals.statusBarMsg.connect(lambda msg: self.statusbar.showMessage(msg, status_bar_message_timeout))
		signals.newWeather.connect(self.updateWeatherIfPrimary)
		signals.kbdPTT.connect(self.setKbdPttState)
		signals.sessionStarted.connect(self.sessionHasStarted)
		signals.sessionEnded.connect(self.sessionHasEnded)
		signals.towerViewProcessToggled.connect(self.towerView_system_action.setChecked)
		signals.towerViewProcessToggled.connect(self.cheat_towerView_menu.setEnabled)
		signals.towerViewProcessToggled.connect(self.towerView_dock.setVisible)
		signals.stripInfoChanged.connect(env.strips.refreshViews)
		signals.fastClockTick.connect(self.updateClock)
		signals.fastClockTick.connect(env.cpdlc.checkForTimeOuts)
		signals.fastClockTick.connect(self.updateRdfSignal)
		signals.slowClockTick.connect(self.checkForRealTimeClockTriggers)
		signals.stripEditRequest.connect(lambda strip: edit_strip(self, strip))
		signals.depClearanceDispRequest.connect(self.depClearanceDispRequested)
		signals.selectionChanged.connect(self.updateStripFplActions)
		signals.receiveStrip.connect(receive_strip)
		signals.cpdlcTransferRequest.connect(receive_CPDLC_transfer_request)
		signals.cpdlcTransferResponse.connect(receive_CPDLC_transfer_response)
		signals.handoverFailure.connect(self.recoverFailedHandover)
		signals.sessionPaused.connect(self.sessionHasPaused)
		signals.sessionResumed.connect(self.sessionHasResumed)
		signals.realTimeSkipped.connect(self.realTimeHasSkipped)
		signals.rackVisibilityLost.connect(self.collectClosedRacks)
		signals.localSettingsChanged.connect(self.localSettingsChanged)
		
		## MISC GUI setup
		self.strip_pane.setViewRacks([default_rack_name]) # will be moved out if a rack panel's saved "visible_racks" claims it [*1]
		self.strip_pane.restoreState(settings.saved_strip_dock_state) # [*1]
		for ptype, ptitle, pstate in settings.saved_user_panels_states:
			panel = user_panels_save_state_kwd[ptype]()
			panel.setWindowTitle(self._unambiguousPanelTitle(ptitle))
			panel.restoreState(pstate)
			panel.show()
			action = self._mkCentralPanelAction(panel, True) # adds an entry to the central panel menu
			if ptitle == settings.saved_selected_docked_panel:
				action.trigger()
		self.rwyBox_pane.setVerticalLayout(settings.vertical_runway_box_layout)
		self.atis_view_action.setEnabled(False)
		self.cheat_towerView_menu.setEnabled(False)
		self.cheat_solo_menu.setEnabled(False)
		self.updateClock()
		self.updateWeatherIfPrimary(settings.primary_METAR_station, None)
		self.updateStripFplActions()
		self.RDF_statusBarLabel.setVisible(settings.radio_direction_finding)
		self.PTT_statusBarLabel.setVisible(False)
		
		self.subsecond_ticker = Ticker(self, signals.fastClockTick.emit)
		self.subminute_ticker = Ticker(self, signals.slowClockTick.emit)
		self.subsecond_ticker.startTicking(subsecond_tick_interval)
		self.subminute_ticker.startTicking(subminute_tick_interval)
		# Disable some base airport stuff if doing CTR
		if env.airport_data is None:
			self.towerView_system_action.setEnabled(False)
			self.adSurfacesUse_options_action.setEnabled(False)
			self.runwayOccupationWarnings_options_action.setEnabled(False)
		# Finish
		if speech_recognition_available:
			prepare_SR_language_files()
		self.applyStylesheet() # do this last to force radar tag boxes to resize appropriately on emitted signal

	def showEvent(self, event):
		QMainWindow.showEvent(self, event)
		if settings.first_time_at_location:
			dialog = LocalSettingsDialog(self)
			dialog.exec()
			if dialog.result() > 0:
				if settings.SSR_mode_capability == '0':
					self._mkCentralPanelAction(LooseStripPane(), True).trigger() # unambiguous only panel title
				else:
					self._mkCentralPanelAction(ScopeFrame(), True).trigger() # unambiguous only panel title

	def _userPanelClosing(self, panel, associated_action):
		self.centralPanelSelection_actionGroup.removeAction(associated_action)
		self.view_centralPanel_menu.removeAction(associated_action)
		self.user_panels = [p for p in self.user_panels if p is not panel]

	def _mkCentralPanelAction(self, panel, is_user_panel):
		action = QAction(panel.windowTitle(), self)
		action.setCheckable(True)
		action.setIcon(panel.windowIcon())
		action.triggered.connect(lambda ignore_checked, p=panel: self.central_workspace.setCurrentPanel(p))
		self.centralPanelSelection_actionGroup.addAction(action)
		self.view_centralPanel_menu.addAction(action)
		if is_user_panel:
			self.user_panels.append(panel)
			panel.closing.connect(lambda p=panel, a=action: self._userPanelClosing(p, a))
			panel.titleChanged.connect(lambda title, a=action: a.setText(title))
		return action

	def _unambiguousPanelTitle(self, base_name):
		new_title = base_name
		i = 2
		while new_title in [panel.windowTitle() for panel in self.user_panels + [self.strip_pane, self.CPDLC_panel, self.teaching_console]]:
			new_title = '%s (%i)' % (base_name, i)
			i += 1
		return new_title

	def flagUnflagSelection(self):
		if selection.acft is not None:
			selection.acft.flagged = not selection.acft.flagged
			signals.selectionChanged.emit()

	def startGuiTimer(self, i, force_start):
		if force_start or not self.alarmClock_statusBarButtons[i].timerIsRunning():
			self.alarmClock_statusBarButtons[i].setTimer()
	
	def goToURL(self, url):
		if not QDesktopServices.openUrl(QUrl(url)):
			QMessageBox.critical(self, 'Error opening web browser', 'Could not invoke desktop web browser.\nGo to: %s' % url)
	
	def applyStylesheet(self):
		try:
			self.setStyleSheet(open(stylesheet_file).read()) # NOTE apparently, we cannot catch syntax errors in stylesheet
			signals.mainStylesheetApplied.emit()
		except FileNotFoundError:
			self.setStyleSheet('')

	def recoverFailedHandover(self, strip, msg):
		recover_strip(strip)
		QMessageBox.critical(self, 'Handover failed', msg)
	
	def localSettingsChanged(self):
		env.rdf.clearAllSignals()
		self.RDF_statusBarLabel.setVisible(settings.radio_direction_finding)
		settings.session_manager.weatherLookUpRequest(settings.primary_METAR_station)

	
	# ---------------------     GUI auto-update functions     ---------------------- #
	
	def updateWindowTitle(self):
		if settings.session_manager.isRunning():
			running_prefix = 'Solo' if settings.session_manager.session_type == SessionType.SOLO else settings.my_callsign
			running_prefix += ' @ '
		else:
			running_prefix = ''
		self.setWindowTitle('ATC-pie - %s%s (%s)' % (running_prefix, env.locationName(), settings.location_code))
	
	def updateClock(self):
		self.clock_statusBarLabel.setText('UTC ' + timestr(seconds=True))
	
	def checkForRealTimeClockTriggers(self):
		if settings.session_manager.isRunning():
			strip_auto_print_check()
			if settings.record_ATIS_reminder is not None and now() > settings.record_ATIS_reminder:
				settings.record_ATIS_reminder = None
				self.notification_pane.notifyTimeForAtis()
	
	def releaseSessionStartTempLock(self):
		settings.session_start_temp_lock = False
	
	def updateWeatherIfPrimary(self, station, weather): # NOTE weather may be None here
		if station == settings.primary_METAR_station:
			# Update status bar info
			self.METAR_statusBarLabel.setText(None if weather is None else weather.METAR())
			self.wind_statusBarLabel.setText('Wind ' + ('---' if weather is None else weather.readWind()))
			qnh = None if weather is None else weather.QNH() # NOTE qnh may still be None
			self.QNH_statusBarLabel.setText('QNH ' + ('---' if qnh is None else ('%d / %.2f' % (qnh, hPa2inHg * qnh))))
			# Update tower view
			if weather is not None and not settings.TWR_view_clear_weather_cheat:
				settings.controlled_tower_viewer.setWeather(weather)

	def setKbdPttState(self, toggle):
		settings.keyboard_PTT_pressed = toggle
		self.updatePTT()

	def updatePTT(self):
		self.PTT_statusBarLabel.setText('PTT' if settings.keyboard_PTT_pressed else ' - - - ')
	
	def updateRdfSignal(self):
		if settings.radio_direction_finding:
			sig = env.rdf.latestLiveSignal()
			if sig is None:
				s1 = ' - - - '
			else:
				s1 = sig.direction.read()
				self.last_RDF_qdm = sig.direction.opposite()
			s2 = ' - - - ' if self.last_RDF_qdm is None else self.last_RDF_qdm.read()
			self.RDF_statusBarLabel.setText('RDF %s / %s' % (s1, s2))
	
	def updateSessionStartStopActions(self):
		running = settings.session_manager.isRunning()
		for gt, ma in {
					SessionType.SOLO: self.soloSession_system_action,
					SessionType.FLIGHTGEAR: self.flightGearSession_system_action,
					SessionType.FSD: self.fsdConnection_system_action,
					SessionType.STUDENT: self.studentSession_system_action,
					SessionType.TEACHER: self.teacherSession_system_action
				}.items():
			if gt == settings.session_manager.session_type:
				ma.setEnabled(True)
				ma.setChecked(running)
			else:
				ma.setEnabled(not running)
				ma.setChecked(False)
	
	def updateStripFplActions(self):
		self.newLinkedStrip_action.setEnabled(selection.strip is None and not selection.acft == selection.fpl is None)
		self.newLinkedFPL_action.setEnabled(selection.strip is not None and selection.strip.linkedFPL() is None)
	
	def sessionHasStarted(self, session_type):
		self.updateWindowTitle()
		self.session_start_temp_lock_timer.start(session_start_temp_lock_duration)
		self.updateSessionStartStopActions()
		self.atis_view_action.setEnabled(env.airport_data is not None)
		self.cheat_solo_menu.setEnabled(session_type == SessionType.SOLO)
		if session_type == SessionType.STUDENT:
			for toggle_action in self.radarCheatMode_cheat_action, self.showAcftCheatToggles_cheat_action:
				toggle_action.setChecked(False)
				toggle_action.setEnabled(False)
		self.updatePTT()
		self.PTT_statusBarLabel.setVisible(True)
		env.radar.startSweeping()
		
	def sessionHasEnded(self):
		env.radar.stopSweeping()
		env.radar.resetContacts()
		env.strips.removeAllStrips()
		env.FPLs.clearFPLs()
		env.rdf.clearAllSignals()
		env.cpdlc.clearHistory()
		env.weather_information.clear()
		self.updateWindowTitle()
		self.updateSessionStartStopActions()
		self.atis_view_action.setEnabled(False)
		self.pauseSimulation_cheat_action.setChecked(False)
		self.cheat_solo_menu.setEnabled(False)
		for toggle_action in self.radarCheatMode_cheat_action, self.showAcftCheatToggles_cheat_action:
			toggle_action.setEnabled(True)
		self.PTT_statusBarLabel.setVisible(False)
		self.last_RDF_qdm = None
		selection.deselect()
		settings.simulation_paused_at = None
		settings.record_ATIS_reminder = None
		print('Session ended.')
	
	def sessionHasPaused(self):
		env.radar.stopSweeping()
		settings.simulation_paused_at = now()
		for b in self.alarmClock_statusBarButtons:
			b.suspendResume()
	
	def sessionHasResumed(self):
		if settings.simulation_paused_at is not None:
			pause_delay = now() - settings.simulation_paused_at
			for acft in settings.session_manager.getAircraft():
				acft.moveHistoryTimesForward(pause_delay)
			if env.airport_data is not None:
				env.airport_data.delayRwySepTimers(pause_delay)
			for b in self.alarmClock_statusBarButtons:
				b.suspendResume()
			settings.simulation_paused_at = None
			env.radar.startSweeping()

	def realTimeHasSkipped(self, time_skipped):
		if env.airport_data is not None:
			env.airport_data.delayRwySepTimers(-time_skipped)
		for b in self.alarmClock_statusBarButtons:
			if b.timerIsRunning():
				b.skipTime(time_skipped)
	
	def aircraftHasDisappeared(self, acft):
		strip = env.linkedStrip(acft)
		if strip is not None:
			signals.linkedContactLost.emit(strip, acft.coords())
			strip.linkAircraft(None)
		if selection.acft is acft:
			if strip is None:
				selection.deselect()
			else: # was linked when lost
				selection.selectStrip(strip)
	
	def collectClosedRacks(self, racks):
		self.strip_pane.setViewRacks(self.strip_pane.getViewRacks() + racks)
	

	
	
	# ---------------------     Session start functions     ---------------------- #
	
	def start_solo(self):
		if env.airport_data is None: # start CTR solo simulation
			n, ok = QInputDialog.getInt(self, 'Solo CTR session', 'Initial traffic count:', value=2, min=0, max=99)
			if ok:
				settings.my_callsign = settings.location_code
				settings.session_manager = SoloSessionManager_CTR(self, n)
				settings.session_manager.start()
		else: # start AD solo simulation
			self.start_solo_AD_dialog.exec()
			if self.start_solo_AD_dialog.result() > 0: # not rejected
				settings.my_callsign = settings.location_code
				settings.session_manager = SoloSessionManager_AD(self, self.start_solo_AD_dialog.chosenInitialTrafficCount())
				settings.session_manager.start()
	
	def start_FlightGearSession(self):
		self.FG_connect_dialog.exec()
		if self.FG_connect_dialog.result() > 0: # not rejected
			settings.my_callsign = self.FG_connect_dialog.chosenCallsign()
			settings.session_manager = FlightGearSessionManager(self)
			settings.session_manager.start()
	
	def start_FSD(self):
		self.FSD_connect_dialog.exec()
		if self.FSD_connect_dialog.result() > 0: # not rejected
			settings.my_callsign = self.FSD_connect_dialog.chosenCallsign()
			settings.session_manager = FsdSessionManager(self)
			settings.session_manager.start()
	
	def start_teaching(self):
		self.start_teacher_session_dialog.exec()
		if self.start_teacher_session_dialog.result() > 0: # not rejected
			settings.my_callsign = teacher_callsign
			settings.session_manager = TeacherSessionManager(self)
			settings.session_manager.start()
	
	def start_learning(self):
		self.start_student_session_dialog.exec()
		if self.start_student_session_dialog.result() > 0: # not rejected
			settings.my_callsign = student_callsign
			settings.session_manager = StudentSessionManager(self)
			settings.session_manager.start()

	
	
	# ---------------------     GUI menu actions     ---------------------- #
	
	## SYSTEM MENU ##
	
	def startStopSession(self, start_func):
		if settings.session_manager.isRunning(): # Stop session
			selection.deselect()
			settings.session_manager.stop()
		else: # Start session
			settings.session_start_temp_lock = True
			start_func()
		self.updateSessionStartStopActions()
		if not settings.session_manager.isRunning():
			settings.session_start_temp_lock = False

	def toggleTowerWindow(self):
		if self.towerView_system_action.isChecked():
			if env.airport_data is not None and len(env.airport_data.viewpoints) == 0:
				QMessageBox.warning(self, 'No viewpoint',
						'Airport data does not specify a viewpoint. ATC-pie will position one near a runway.\n'
						'Update your source file if one should be available.')
			settings.controlled_tower_viewer.start()
		else:
			settings.controlled_tower_viewer.stop()

	def selectIndicateViewpoint(self, vp_index):
		if vp_index != settings.selected_viewpoint:
			settings.selected_viewpoint = vp_index
			settings.tower_height_cheat_offset = 0
			if settings.controlled_tower_viewer.isRunning():
				settings.controlled_tower_viewer.updateTowerPosition()
		signals.indicatePoint.emit(env.viewpoint()[0])

	def configureAdditionalViewers(self):
		AdditionalViewersDialog(self).exec()

	def reloadBackgroundImages(self):
		print('Reload: background images')
		settings.radar_background_images, settings.loose_strip_bay_backgrounds = read_bg_img(settings.location_code, env.navpoints)
		signals.backgroundImagesReloaded.emit()
		QMessageBox.information(self, 'Done reloading', 'Background images reloaded. Check for console error messages.')

	def reloadFgAcftModels(self):
		print('Reload: FG aircraft models')
		make_FGFS_models_liveries()
		QMessageBox.information(self, 'Done reloading', 'FG aircraft models reloaded. Check for console error messages.')
	
	def reloadStylesheetAndColours(self):
		print('Reload: stylesheet and colours')
		self.applyStylesheet() # emits its own signal
		settings.loadColourSettings()
		signals.colourConfigReloaded.emit()
		QMessageBox.information(self, 'Done reloading', 'Stylesheet and colour configuration reloaded. Check for console error messages.')
	
	def reloadRoutePresetsAndEntryExitPoints(self):
		print('Reload: route presets and AD entry/exit points')
		settings.route_presets = read_route_presets(stderr)
		world_routing_db.clearEntryExitPoints()
		import_entry_exit_data(stderr)
		QMessageBox.information(self, 'Done reloading', 'Route presets and entry/exit points reloaded. Check for console error messages.')
	
	def switchMeasuringCoordsLog(self, toggle):
		settings.measuring_tool_logs_coordinates = toggle
		self.radar_measurement_log.setVisible(toggle)
	
	def extractSectorFile(self):
		txt, ignore = QFileDialog.getOpenFileName(self, caption='Select sector file to extract from')
		if txt != '':
			extract_sector(txt, env.radarPos(), settings.map_range)
			QMessageBox.information(self, 'Done extracting',
				'Background drawings extracted.\nSee console for summary and files created in the "OUTPUT" directory.')
	
	def repositionRadarBgImages(self):
		w = self.central_workspace.currentPanel()
		if isinstance(w, ScopeFrame):
			w.positionVisibleBgImages()
		else:
			QMessageBox.critical(self, 'Image positioning error', 'This requires a radar panel docked in the central area.')
	
	def openSoloSessionSettings(self):
		SoloSessionSettingsDialog(self).exec()
	
	def openLocalSettings(self):
		dialog = LocalSettingsDialog(self)
		dialog.exec()
		if dialog.result() > 0 and settings.session_manager.isRunning():
			env.radar.startSweeping()
	
	def openSystemSettings(self):
		SystemSettingsDialog(self).exec()
	
	def changeLocation(self):
		if QMessageBox.question(self, 'Change location', 'This will close the current session. Are you sure?') == QMessageBox.Yes:
			self.launcher.show()
			self.close()
	
	
	## VIEW MENU ##

	def recallWindowState(self):
		try:
			with open(dock_layout_file, 'rb') as f:
				self.restoreState(f.read())
		except FileNotFoundError:
			QMessageBox.critical(self, 'Recall dock layout', 'No saved layout to recall.')

	def saveDockLayout(self): # STYLE catch file write error
		with open(dock_layout_file, 'wb') as f:
			f.write(self.saveState())
		QMessageBox.information(self, 'Save dock layout', 'Current dock layout saved.')

	def newUserPanel(self, panel):
		panel.setWindowTitle(self._unambiguousPanelTitle(panel.windowTitle()))
		panel.show()
		txt, ok = QInputDialog.getText(panel, 'New panel', 'New panel title:', text=panel.windowTitle())
		if ok:
			if txt == '':
				txt = panel.defaultTitle()
			panel.setWindowTitle(self._unambiguousPanelTitle(txt))
			self._mkCentralPanelAction(panel, True)
		else:
			panel.close()

	def showRaisePanel(self, panel):
		if self.central_workspace.currentPanel() is panel:
			self.raise_()
		elif panel.isVisible(): # panel is popped out
			panel.raise_()
		else: # panel is closed
			panel.show()
		flash_widget(panel, panel.flashStyleSheet())
		panel.setFocus()

	def depClearanceDispRequested(self, strip):
		self.DEP_clearance_view.updateView(strip)
		open_raise_window(self.DEP_clearance_view)

	def openSelectedStrip(self):
		if selection.strip is None:
			QMessageBox.critical(self, 'Strip detail sheet', 'No strip in selection.')
		else:
			signals.stripEditRequest.emit(selection.strip)

	def showLastCpdlcDialogueForSelection(self):
		cs = selection.selectedCallsign()
		if cs is None:
			QMessageBox.critical(self, 'Last CPDLC dialogue', 'No callsign in selection.')
		else:
			signals.cpdlcWindowRequest.emit(cs, False)

	def showDepClearanceForSelection(self):
		if selection.strip is None:
			QMessageBox.critical(self, 'Departure clearance', 'No strip in selection.')
		elif selection.strip.lookup(departure_clearance_detail):
			signals.depClearanceDispRequest.emit(selection.strip)
		elif QMessageBox.question(self, 'Departure clearance', 'No departure clearance registered on strip.\nPrepare one now?') == QMessageBox.Yes:
			DepartureClearanceEditDialog(self, selection.strip).exec()

	def openAtisDialog(self):
		if settings.session_manager.isRunning() and env.airport_data is not None:
			AtisDialog(self).exec()
	
	
	## OPTIONS MENU ##
	
	def configureAdSfcUse(self):
		AdSfcUseDialog(self).exec()
	
	def muteNotificationSounds(self, toggle):
		settings.mute_notifications = toggle
	
	def switchPrimaryRadar(self, toggle):
		settings.primary_radar_active = toggle
		env.radar.instantSweep()
	
	def switchConflictWarnings(self, toggle):
		settings.route_conflict_warnings = toggle
		env.radar.checkPositionRouteConflicts()
	
	def switchTrafficIdentification(self, toggle):
		settings.traffic_identification_assistant = toggle
		if not toggle:
			for strip in env.strips.listAll():
				strip.writeDetail(soft_link_detail, None)
			signals.stripInfoChanged.emit() # to trigger refreshViews and global radar checks
	
	def switchRwyOccupationIndications(self, toggle):
		settings.monitor_runway_occupation = toggle
	
	def switchApproachSpacingHints(self, toggle):
		settings.APP_spacing_hints = toggle
		signals.stripInfoChanged.emit()
	
	def openGeneralSettings(self):
		GeneralSettingsDialog(self).exec()
	
	
	## CHEAT MENU ##
	
	def pauseSession(self, toggle):
		if toggle:
			settings.session_manager.pauseSession()
		else:
			settings.session_manager.resumeSession()

	def skipTimeForwardOnce(self):
		if settings.session_manager.session_type == SessionType.SOLO \
				or settings.session_manager.session_type == SessionType.TEACHER: # allows kbd shortcut while teaching
			settings.session_manager.skipTimeForward(forward_time_skip)
	
	def spawnAircraft(self):
		n, ok = QInputDialog.getInt(self, 'Spawn new aircraft', 'Try to spawn:', value=1, min=1, max=99)
		if ok:
			for i in range(n): # WARNING: session should be running
				settings.session_manager.spawnNewControlledAircraft()
	
	def killSelectedAircraft(self):
		to_kill = selection.acft
		if to_kill is None:
			QMessageBox.critical(self, 'Cheat error', 'No aircraft selected.')
		else:
			kill_aircraft(to_kill)
	
	def setRejectedInstrPopUp(self, toggle):
		settings.solo_erroneous_instruction_warning = toggle
	
	def ensureClearWeather(self, toggle):
		settings.TWR_view_clear_weather_cheat = toggle
		if toggle:
			weather = mkWeather(settings.location_code) # clear weather with location code as station
			settings.controlled_tower_viewer.setWeather(weather)
		else:
			weather = env.primaryWeather()
			if weather is not None:
				settings.controlled_tower_viewer.setWeather(weather)
	
	def setShowRecognisedVoiceStrings(self, toggle):
		settings.show_recognised_voice_strings = toggle
	
	def changeTowerHeight(self):
		try:
			viewpoint_height = env.airport_data.viewpoints[settings.selected_viewpoint][1]
			original_info = 'original value is %d' % viewpoint_height
		except IndexError: # when no viewpoint is specified in source data file
			viewpoint_height = default_tower_height
			original_info = 'no original viewpoint in source data'
		new_height, ok = QInputDialog.getInt(self, 'Cheat tower height', 'New height in feet (%s):' % original_info,
				value=(int(viewpoint_height) + settings.tower_height_cheat_offset), min=10, max=1500, step=10)
		if ok:
			settings.tower_height_cheat_offset = new_height - viewpoint_height
			settings.controlled_tower_viewer.updateTowerPosition()
	
	def setRadarCheatMode(self, toggle):
		settings.radar_cheat = toggle
		env.radar.instantSweep()
	
	def showAcftCheatToggles(self, toggle):
		self.selection_info_pane.showCheatToggle(toggle)
		self.selectionInfo_toolbarWidget.showCheatToggle(toggle)

	
	# -----------------     Internal GUI events      ------------------ #
	
	def closeEvent(self, event):
		env.radar.stopSweeping()
		if settings.session_manager.isRunning():
			settings.session_manager.stop()
		if settings.controlled_tower_viewer.isRunning():
			settings.controlled_tower_viewer.stop(wait=True)
		if speech_recognition_available:
			cleanup_SR_language_files()
		print('Closing main window.')
		settings.saved_strip_racks = env.strips.rackNames()
		settings.saved_strip_dock_state = self.strip_pane.stateSave()
		settings.saved_user_panels_states = [(stateSaveKwd(p), p.windowTitle(), p.stateSave()) for p in self.user_panels]
		settings.saved_selected_docked_panel = None if self.centralPanel_selectNone_action.isChecked() else self.central_workspace.currentPanel().windowTitle()
		signals.mainWindowClosing.emit()
		signals.disconnect()
		settings.saveGeneralAndSystemSettings()
		settings.saveLocalSettings(env.airport_data)
		env.resetEnv()
		settings.resetForNewWindow()
		EarthCoords.clearRadarPos()
		QMainWindow.closeEvent(self, event)
