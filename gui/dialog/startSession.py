
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QMessageBox
from ui.startSoloDialog_AD import Ui_startSoloDialog_AD
from ui.startFgSessionDialog import Ui_startFgSessionDialog
from ui.startFsdDialog import Ui_startFsdDialog
from ui.startStudentSessionDialog import Ui_startStudentSessionDialog
from ui.startTeacherSessionDialog import Ui_startTeacherSessionDialog

from base.util import INET_addr_str

from ext.irc import IRC_available
from ext.mumble import mumble_available

from session.config import settings
from session.env import env
from session.managers.teacher import pyaudio_available


# ---------- Constants ----------

# -------------------------------


class StartSoloDialog_AD(QDialog, Ui_startSoloDialog_AD):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		if env.airport_data is not None and len(env.airport_data.ground_net.taxiways()) > 0 \
				and len(env.airport_data.ground_net.parkingPositions()) > 0: # GND can be enabled
			self.GND_tickBox.toggled.connect(self.updateOKbutton)
		else: # GND must be disabled
			self.GND_tickBox.setEnabled(False)
			self.GND_tickBox.setToolTip('Missing parking positions or taxi routes.')
		self.TWR_tickBox.toggled.connect(self.updateOKbutton)
		self.APP_tickBox.toggled.connect(self.updateOKbutton)
		self.DEP_tickBox.toggled.connect(self.updateOKbutton)
		self.updateOKbutton()
		self.buttonBox.accepted.connect(self.doOK)
		self.buttonBox.rejected.connect(self.reject)
	
	def updateOKbutton(self):
		gnd, twr, app, dep = (box.isChecked() for box in [self.GND_tickBox, self.TWR_tickBox, self.APP_tickBox, self.DEP_tickBox])
		self.buttonBox.button(QDialogButtonBox.Ok).setEnabled((gnd or twr or app or dep) and (not gnd or twr or not app and not dep))
	
	def doOK(self):
		settings.solo_role_GND = self.GND_tickBox.isChecked()
		settings.solo_role_TWR = self.TWR_tickBox.isChecked()
		settings.solo_role_APP = self.APP_tickBox.isChecked()
		settings.solo_role_DEP = self.DEP_tickBox.isChecked()
		self.accept()
	
	def chosenInitialTrafficCount(self):
		return self.initTrafficCount_edit.value()




class StartFgSessionDialog(QDialog, Ui_startFgSessionDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.irc_subSystem_tickBox.setEnabled(IRC_available)
		self.mumble_subSystem_tickBox.setEnabled(mumble_available)
		self.buttonBox.accepted.connect(self.doOK)
		self.buttonBox.rejected.connect(self.reject)
	
	def showEvent(self, event):
		self.fgmsServer_info.setText(INET_addr_str(settings.FGMS_server_host, settings.FGMS_server_port))
		self.callsign_edit.setText(settings.location_code + 'obs') # should contain no whitespace, cf. use with IRC
		self.clientPort_edit.setValue(settings.FGMS_client_port)
		self.irc_subSystem_tickBox.setChecked(IRC_available and settings.FG_IRC_enabled)
		self.mumble_subSystem_tickBox.setChecked(mumble_available and settings.phone_lines_enabled)
		self.orsx_subSystem_tickBox.setChecked(settings.FG_ORSX_enabled)
		self.callsign_edit.setFocus()
	
	def doOK(self):
		cs = self.callsign_edit.text()
		if cs == '' or ' ' in cs or ',' in cs:
			QMessageBox.critical(self, 'FGMS start error', 'Invalid callsign.')
		elif settings.MP_social_name == '':
			QMessageBox.critical(self, 'FGMS start error', 'No social name set; please edit system settings.')
		elif self.irc_subSystem_tickBox.isChecked() and settings.FG_IRC_channel == '':
			QMessageBox.critical(self, 'FGMS start error', 'IRC channel required for native ATC-pie messaging; please edit system settings.')
		elif self.mumble_subSystem_tickBox.isChecked() and settings.land_line_server_host == '':
			QMessageBox.critical(self, 'FGMS start error', 'Mumble server required for ATC phone lines; please edit system settings.')
		elif self.orsx_subSystem_tickBox.isChecked() and settings.ORSX_server_name == '':
			QMessageBox.critical(self, 'FGMS start error', 'OpenRadar server address missing in the system settings.')
		else: # all OK; update settings and accept dialog
			settings.FGMS_client_port = self.clientPort_edit.value()
			settings.FG_IRC_enabled = self.irc_subSystem_tickBox.isChecked()
			settings.phone_lines_enabled = self.mumble_subSystem_tickBox.isChecked()
			settings.FG_ORSX_enabled = self.orsx_subSystem_tickBox.isChecked()
			self.accept()
	
	def chosenCallsign(self):
		return self.callsign_edit.text()




class StartFsdDialog(QDialog, Ui_startFsdDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.use_landlines_tickBox.setEnabled(mumble_available)
		self.buttonBox.accepted.connect(self.doOK)
		self.buttonBox.rejected.connect(self.reject)
	
	def showEvent(self, event):
		self.fsdServer_info.setText(INET_addr_str(settings.FSD_server_host, settings.FSD_server_port))
		self.callsign_edit.setText(settings.location_code)
		self.visibilityRange_edit.setValue(settings.FSD_visibility_range)
		self.fgcom_port_edit.setValue(settings.FSD_FGCom_port)
		self.use_landlines_tickBox.setChecked(mumble_available and settings.phone_lines_enabled)
		self.use_FGComMumble_tickBox.setChecked(mumble_available and settings.FSD_FGCom_enabled)
		self.use_HoppieAcars_tickBox.setChecked(settings.FSD_Hoppie_enabled)
	
	def doOK(self):
		cs = self.callsign_edit.text()
		if cs == '' or ' ' in cs or ':' in cs:
			QMessageBox.critical(self, 'FSD start error', 'Invalid callsign.')
		elif settings.MP_social_name == '' or ':' in settings.MP_social_name:
			QMessageBox.critical(self, 'FSD start error', 'Missing or invalid social name; please edit system settings.')
		elif self.use_landlines_tickBox.isChecked() and settings.land_line_server_host == '':
			QMessageBox.critical(self, 'FSD start error', 'Mumble service required for ATC phone lines; please edit system settings.')
		elif self.use_FGComMumble_tickBox.isChecked() and settings.FGCom_mumble_host == '':
			QMessageBox.critical(self, 'FSD start error', 'No client given for FGCom-mumble; please edit system settings.')
		elif self.use_HoppieAcars_tickBox.isChecked() and settings.FSD_Hoppie_logon == '':
			QMessageBox.critical(self, 'FSD start error', 'Hoppie logon code required; please edit system settings.')
		else: # all OK; accept dialog
			settings.FSD_visibility_range = self.visibilityRange_edit.value()
			settings.FSD_FGCom_port = self.fgcom_port_edit.value()
			settings.phone_lines_enabled = self.use_landlines_tickBox.isChecked()
			settings.FSD_FGCom_enabled = self.use_FGComMumble_tickBox.isChecked()
			settings.FSD_Hoppie_enabled = self.use_HoppieAcars_tickBox.isChecked()
			self.accept()
	
	def chosenCallsign(self):
		return self.callsign_edit.text()




class StartStudentSessionDialog(QDialog, Ui_startStudentSessionDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.teachingServiceHost_edit.setText(settings.teaching_service_host)
		self.teachingServicePort_edit.setValue(settings.teaching_service_port)
		self.updateOKbutton()
		self.teachingServiceHost_edit.textChanged.connect(self.updateOKbutton)
		self.buttonBox.accepted.connect(self.doOK)
		self.buttonBox.rejected.connect(self.reject)
	
	def updateOKbutton(self):
		self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(self.teachingServiceHost_edit.text() != '')
	
	def doOK(self):
		settings.teaching_service_host = self.teachingServiceHost_edit.text()
		settings.teaching_service_port = self.teachingServicePort_edit.value()
		self.accept()




class StartTeacherSessionDialog(QDialog, Ui_startTeacherSessionDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.usePyAudio_tickBox.setEnabled(pyaudio_available)
		self.teachingServicePort_edit.setValue(settings.teaching_service_port)
		self.buttonBox.accepted.connect(self.doOK)
		self.buttonBox.rejected.connect(self.reject)

	def showEvent(self, event):
		self.usePyAudio_tickBox.setChecked(pyaudio_available and settings.phone_lines_enabled)

	def doOK(self):
		settings.teaching_service_port = self.teachingServicePort_edit.value()
		settings.phone_lines_enabled = self.usePyAudio_tickBox.isChecked()
		self.accept()
