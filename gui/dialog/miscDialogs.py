
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QMessageBox, QInputDialog, QPlainTextEdit, QDialogButtonBox, QVBoxLayout, QLabel

from ui.aboutDialog import Ui_aboutDialog
from ui.routeSpecsLostDialog import Ui_routeSpecsLostDialog
from ui.discardedStripsDialog import Ui_discardedStripsDialog
from ui.editRackDialog import Ui_editRackDialog
from ui.cpdlcXfrOptionsDialog import Ui_cpdlcXfrOptionsDialog

from base.cpdlc import CPDLC_element_display_text
from base.strip import rack_detail, recycled_detail

from session.config import settings, version_string
from session.env import env
from session.models.liveStrips import default_rack_name

from gui.misc import signals, IconFile, RadioKeyEventFilter


# ---------- Constants ----------

version_string_placeholder = '##version##'

# -------------------------------


def hostPort_input(parent, title, prompt):
	"""
	returns (host, port) pair, with None host if input cancelled by user.
	"""
	text, ok = QInputDialog.getText(parent, title, prompt)
	while ok:
		split = text.rsplit(':', maxsplit=1)
		if len(split) == 2 and len(split[0]) > 0 and split[1].isdecimal():
			return split[0], int(split[1])
		QMessageBox.critical(parent, title, 'Bad "host:port" format.')
		text, ok = QInputDialog.getText(parent, title, prompt)
	return None, None



class TextInputDialog(QDialog):
	def __init__(self, parent, title, label, suggestion=''):
		QDialog.__init__(self, parent)
		self.setWindowTitle(title)
		self._result = None
		self.prompt_label = QLabel(label, self)
		self.txt_edit = QPlainTextEdit(suggestion, self)
		self.txt_edit.setTabChangesFocus(True)
		self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, self)
		self.layout = QVBoxLayout(self)
		self.layout.addWidget(self.prompt_label)
		self.layout.addWidget(self.txt_edit)
		self.layout.addWidget(self.button_box)
		self.button_box.accepted.connect(self.doAccept)
		self.button_box.rejected.connect(self.doReject)

	def doAccept(self):
		self._result = self.txt_edit.toPlainText()
		self.accept()

	def doReject(self):
		self._result = None
		self.reject()

	def textResult(self):
		return self._result




class AboutDialog(QDialog, Ui_aboutDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.text_browser.setHtml(re.sub(version_string_placeholder, version_string, self.text_browser.toHtml()))




class RadarMeasurementLog(QPlainTextEdit):
	def __init__(self, parent=None, visibilityAction=None):
		QPlainTextEdit.__init__(self, parent)
		self.setWindowTitle('Radar measurements log')
		self.setWindowFlags(Qt.Window)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.hide()
		self.visibility_action = visibilityAction
		signals.measuringLogEntry.connect(self.appendEntry)
		signals.closeNonDockableWindows.connect(self.close)

	def appendEntry(self, log_entry):
		txt = self.toPlainText()
		self.setPlainText(txt + '\n' + log_entry if txt else log_entry)
		self.verticalScrollBar().setValue(self.verticalScrollBar().maximum())

	def showEvent(self, event):
		QPlainTextEdit.showEvent(self, event)
		self.visibility_action.setChecked(True)

	def hideEvent(self, event):
		QPlainTextEdit.hideEvent(self, event)
		self.visibility_action.setChecked(False)




class CpdlcXfrOptionsDialog(QDialog, Ui_cpdlcXfrOptionsDialog):
	def __init__(self, parent, handover_msg_elt):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.contactInstruction_info.setText(CPDLC_element_display_text(handover_msg_elt))

	def transferOptionSelected(self):
		return self.cpdlcTransfer_option.isChecked()

	def instructionOptionSelected(self):
		return self.contactInstruction_option.isChecked()



class RouteSpecsLostDialog(QDialog, Ui_routeSpecsLostDialog):
	def __init__(self, parent, title, lost_specs_text):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowTitle(title)
		self.lostSpecs_box.setText(lost_specs_text)
	
	def mustOpenStripDetails(self):
		return self.openStripDetailSheet_tickBox.isChecked()



class DiscardedStripsDialog(QDialog, Ui_discardedStripsDialog):
	def __init__(self, parent, view_model, dialog_title):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.clear_button.setIcon(QIcon(IconFile.button_clear))
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowTitle(dialog_title)
		self.model = view_model
		self.strip_view.setModel(view_model)
		self.clear_button.clicked.connect(self.model.forgetStrips)
		self.recall_button.clicked.connect(self.recallSelectedStrips)
		self.close_button.clicked.connect(self.accept)
	
	def recallSelectedStrips(self):
		strips = [self.model.stripAt(index) for index in self.strip_view.selectedIndexes()]
		if strips:
			for strip in strips:
				signals.stripRecall.emit(strip)
			self.accept()



class EditRackDialog(QDialog, Ui_editRackDialog):
	def __init__(self, parent, rack_name):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.deleteRack_info.clear()
		self.installEventFilter(RadioKeyEventFilter(self))
		self.initial_rack_name = rack_name
		self.flagged_for_deletion = False
		self.rackName_edit.setText(self.initial_rack_name)
		self.privateRack_tickBox.setChecked(self.initial_rack_name in settings.private_racks)
		self.pickColour_widget.setChoice(settings.rack_colours.get(rack_name, None))
		if rack_name == default_rack_name:
			self.rackName_edit.setEnabled(False)
			self.collectedStrips_box.setVisible(False)
			self.deleteRack_button.setEnabled(False)
			self.deleteRack_info.setText('Default rack cannot be deleted')
		else:
			self.collectsFrom_edit.setPlainText('\n'.join(atc for atc, rack in settings.ATC_collecting_racks.items() if rack == rack_name))
			self.collectAutoPrintedStrips_tickBox.setChecked(settings.auto_print_collecting_rack == self.initial_rack_name)
			self.rackName_edit.selectAll()
			self.deleteRack_button.toggled.connect(self.flagRackForDeletion)
		self.buttonBox.rejected.connect(self.reject)
		self.buttonBox.accepted.connect(self.doOK)

	def flagRackForDeletion(self, toggle):
		if toggle:
			if env.strips.count(lambda s: s.lookup(rack_detail) == self.initial_rack_name) > 0:
				QMessageBox.warning(self, 'Non-empty rack deletion', 'Rack not empty. Strips will be reracked if deletion confirmed.')
			self.deleteRack_info.setText('Flagged for deletion')
		else:
			self.deleteRack_info.clear()
	
	def doOK(self):
		if self.deleteRack_button.isChecked():
			for strip in env.strips.listAll():
				if strip.lookup(rack_detail) == self.initial_rack_name:
					strip.writeDetail(recycled_detail, True)
					env.strips.repositionStrip(strip, default_rack_name)
			for atc, rack in list(settings.ATC_collecting_racks.items()):
				if rack == self.initial_rack_name:
					del settings.ATC_collecting_racks[atc]
			if settings.auto_print_collecting_rack == self.initial_rack_name:
				settings.auto_print_collecting_rack = None
			env.strips.removeRack(self.initial_rack_name)
		else: # rack NOT being deleted
			new_name = self.rackName_edit.text()
			# UPDATE SETTINGS
			if new_name != self.initial_rack_name: # renaming
				if env.strips.validNewRackName(new_name):
					env.strips.renameRack(self.initial_rack_name, new_name)
				else:
					QMessageBox.critical(self, 'Rack name error', 'Name is reserved or already used.')
					return # abort
			# private
			if self.initial_rack_name in settings.private_racks:
				settings.private_racks.remove(self.initial_rack_name)
			if self.privateRack_tickBox.isChecked():
				settings.private_racks.add(new_name)
			# colour
			new_colour = self.pickColour_widget.getChoice()
			if self.initial_rack_name in settings.rack_colours:
				del settings.rack_colours[self.initial_rack_name]
			if new_colour is not None:
				settings.rack_colours[new_name] = new_colour
			# collecting racks
			for atc, rack in list(settings.ATC_collecting_racks.items()):
				if rack == self.initial_rack_name:
					del settings.ATC_collecting_racks[atc]
			for atc in self.collectsFrom_edit.toPlainText().split('\n'):
				if atc != '':
					settings.ATC_collecting_racks[atc] = new_name
			if self.collectAutoPrintedStrips_tickBox.isChecked(): # should not be ticked if default rack
				settings.auto_print_collecting_rack = new_name
			elif settings.auto_print_collecting_rack == self.initial_rack_name:
				settings.auto_print_collecting_rack = None # back to default if box has been unticked
			# DONE
			signals.rackEdited.emit(self.initial_rack_name, new_name)
		self.accept()
