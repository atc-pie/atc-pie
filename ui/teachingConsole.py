# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'teachingConsole.ui'
#
# Created by: PyQt5 UI code generator 5.15.10
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_teachingConsole(object):
    def setupUi(self, teachingConsole):
        teachingConsole.setObjectName("teachingConsole")
        teachingConsole.resize(868, 485)
        self.gridLayout_3 = QtWidgets.QGridLayout(teachingConsole)
        self.gridLayout_3.setSpacing(15)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.selectedAircraft_box = QtWidgets.QGroupBox(teachingConsole)
        self.selectedAircraft_box.setObjectName("selectedAircraft_box")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.selectedAircraft_box)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.acftCallsign_info = QtWidgets.QLabel(self.selectedAircraft_box)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        self.acftCallsign_info.setFont(font)
        self.acftCallsign_info.setObjectName("acftCallsign_info")
        self.horizontalLayout_2.addWidget(self.acftCallsign_info)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.spawn_button = QtWidgets.QToolButton(self.selectedAircraft_box)
        font = QtGui.QFont()
        font.setBold(True)
        self.spawn_button.setFont(font)
        self.spawn_button.setObjectName("spawn_button")
        self.horizontalLayout_2.addWidget(self.spawn_button)
        self.freezeACFT_button = QtWidgets.QToolButton(self.selectedAircraft_box)
        self.freezeACFT_button.setCheckable(True)
        self.freezeACFT_button.setObjectName("freezeACFT_button")
        self.horizontalLayout_2.addWidget(self.freezeACFT_button)
        self.kill_button = QtWidgets.QToolButton(self.selectedAircraft_box)
        self.kill_button.setObjectName("kill_button")
        self.horizontalLayout_2.addWidget(self.kill_button)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        spacerItem1 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_2.addItem(spacerItem1)
        self.groupBox_2 = QtWidgets.QGroupBox(self.selectedAircraft_box)
        self.groupBox_2.setObjectName("groupBox_2")
        self.formLayout_2 = QtWidgets.QFormLayout(self.groupBox_2)
        self.formLayout_2.setObjectName("formLayout_2")
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setObjectName("label")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.xpdrMode_select = QtWidgets.QComboBox(self.groupBox_2)
        self.xpdrMode_select.setObjectName("xpdrMode_select")
        self.xpdrMode_select.addItem("")
        self.xpdrMode_select.addItem("")
        self.xpdrMode_select.addItem("")
        self.xpdrMode_select.addItem("")
        self.horizontalLayout.addWidget(self.xpdrMode_select)
        spacerItem2 = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.squat_tickBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.squat_tickBox.setObjectName("squat_tickBox")
        self.horizontalLayout.addWidget(self.squat_tickBox)
        self.formLayout_2.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout)
        self.label_2 = QtWidgets.QLabel(self.groupBox_2)
        self.label_2.setObjectName("label_2")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.label_3 = QtWidgets.QLabel(self.groupBox_2)
        self.label_3.setObjectName("label_3")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.xpdrIdent_tickBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.xpdrIdent_tickBox.setCheckable(True)
        self.xpdrIdent_tickBox.setObjectName("xpdrIdent_tickBox")
        self.horizontalLayout_4.addWidget(self.xpdrIdent_tickBox)
        self.squawkVFR_button = QtWidgets.QToolButton(self.groupBox_2)
        self.squawkVFR_button.setObjectName("squawkVFR_button")
        self.horizontalLayout_4.addWidget(self.squawkVFR_button)
        self.pushSQtoStrip_button = QtWidgets.QToolButton(self.groupBox_2)
        self.pushSQtoStrip_button.setObjectName("pushSQtoStrip_button")
        self.horizontalLayout_4.addWidget(self.pushSQtoStrip_button)
        self.formLayout_2.setLayout(2, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_4)
        self.xpdrCode_select = XpdrCodeSelectorWidget(self.groupBox_2)
        self.xpdrCode_select.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.xpdrCode_select.setObjectName("xpdrCode_select")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.xpdrCode_select)
        self.verticalLayout_2.addWidget(self.groupBox_2)
        self.groupBox_5 = QtWidgets.QGroupBox(self.selectedAircraft_box)
        self.groupBox_5.setObjectName("groupBox_5")
        self.formLayout_3 = QtWidgets.QFormLayout(self.groupBox_5)
        self.formLayout_3.setObjectName("formLayout_3")
        self.label_6 = QtWidgets.QLabel(self.groupBox_5)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)
        self.label_6.setObjectName("label_6")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_6)
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.cpdlcStatus_info = QtWidgets.QLabel(self.groupBox_5)
        self.cpdlcStatus_info.setObjectName("cpdlcStatus_info")
        self.horizontalLayout_14.addWidget(self.cpdlcStatus_info)
        self.showDataLinkWindow_button = QtWidgets.QToolButton(self.groupBox_5)
        self.showDataLinkWindow_button.setObjectName("showDataLinkWindow_button")
        self.horizontalLayout_14.addWidget(self.showDataLinkWindow_button)
        self.formLayout_3.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_14)
        self.label_8 = QtWidgets.QLabel(self.groupBox_5)
        self.label_8.setObjectName("label_8")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_8)
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.cpdlcLogOn_button = QtWidgets.QToolButton(self.groupBox_5)
        self.cpdlcLogOn_button.setObjectName("cpdlcLogOn_button")
        self.horizontalLayout_8.addWidget(self.cpdlcLogOn_button)
        self.cpdlcTransfer_button = QtWidgets.QToolButton(self.groupBox_5)
        self.cpdlcTransfer_button.setObjectName("cpdlcTransfer_button")
        self.horizontalLayout_8.addWidget(self.cpdlcTransfer_button)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem3)
        self.formLayout_3.setLayout(1, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_8)
        self.verticalLayout_2.addWidget(self.groupBox_5)
        self.onTouchDown_groupBox = QtWidgets.QGroupBox(self.selectedAircraft_box)
        self.onTouchDown_groupBox.setObjectName("onTouchDown_groupBox")
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout(self.onTouchDown_groupBox)
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.onTouchDown_land_radioButton = QtWidgets.QRadioButton(self.onTouchDown_groupBox)
        self.onTouchDown_land_radioButton.setObjectName("onTouchDown_land_radioButton")
        self.horizontalLayout_9.addWidget(self.onTouchDown_land_radioButton)
        self.onTouchDown_touchAndGo_radioButton = QtWidgets.QRadioButton(self.onTouchDown_groupBox)
        self.onTouchDown_touchAndGo_radioButton.setObjectName("onTouchDown_touchAndGo_radioButton")
        self.horizontalLayout_9.addWidget(self.onTouchDown_touchAndGo_radioButton)
        self.onTouchDown_skidOffRwy_radioButton = QtWidgets.QRadioButton(self.onTouchDown_groupBox)
        self.onTouchDown_skidOffRwy_radioButton.setObjectName("onTouchDown_skidOffRwy_radioButton")
        self.horizontalLayout_9.addWidget(self.onTouchDown_skidOffRwy_radioButton)
        self.verticalLayout_2.addWidget(self.onTouchDown_groupBox)
        self.onTouchDown_groupBox.raise_()
        self.groupBox_2.raise_()
        self.groupBox_5.raise_()
        self.gridLayout_3.addWidget(self.selectedAircraft_box, 0, 0, 3, 1)
        self.tabWidget = QtWidgets.QTabWidget(teachingConsole)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.tab_2)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.groupBox_4 = QtWidgets.QGroupBox(self.tab_2)
        self.groupBox_4.setObjectName("groupBox_4")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox_4)
        self.gridLayout.setObjectName("gridLayout")
        self.windCalm_radioButton = QtWidgets.QRadioButton(self.groupBox_4)
        self.windCalm_radioButton.setChecked(True)
        self.windCalm_radioButton.setObjectName("windCalm_radioButton")
        self.gridLayout.addWidget(self.windCalm_radioButton, 0, 0, 1, 1)
        self.windDirection_widget = QtWidgets.QWidget(self.groupBox_4)
        self.windDirection_widget.setEnabled(False)
        self.windDirection_widget.setObjectName("windDirection_widget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.windDirection_widget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.windHdg_dial = QtWidgets.QDial(self.windDirection_widget)
        self.windHdg_dial.setMinimum(0)
        self.windHdg_dial.setMaximum(72)
        self.windHdg_dial.setSingleStep(5)
        self.windHdg_dial.setProperty("value", 36)
        self.windHdg_dial.setWrapping(True)
        self.windHdg_dial.setNotchesVisible(True)
        self.windHdg_dial.setObjectName("windHdg_dial")
        self.gridLayout_2.addWidget(self.windHdg_dial, 0, 0, 2, 1)
        self.windHdgRange_enable = QtWidgets.QCheckBox(self.windDirection_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.windHdgRange_enable.sizePolicy().hasHeightForWidth())
        self.windHdgRange_enable.setSizePolicy(sizePolicy)
        self.windHdgRange_enable.setObjectName("windHdgRange_enable")
        self.gridLayout_2.addWidget(self.windHdgRange_enable, 0, 1, 1, 1)
        self.windHdgRange_edit = QtWidgets.QSpinBox(self.windDirection_widget)
        self.windHdgRange_edit.setEnabled(False)
        self.windHdgRange_edit.setMinimum(10)
        self.windHdgRange_edit.setMaximum(80)
        self.windHdgRange_edit.setSingleStep(5)
        self.windHdgRange_edit.setObjectName("windHdgRange_edit")
        self.gridLayout_2.addWidget(self.windHdgRange_edit, 1, 1, 1, 1)
        self.gridLayout.addWidget(self.windDirection_widget, 0, 1, 3, 1)
        self.windGusts_widget = QtWidgets.QWidget(self.groupBox_4)
        self.windGusts_widget.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.windGusts_widget.sizePolicy().hasHeightForWidth())
        self.windGusts_widget.setSizePolicy(sizePolicy)
        self.windGusts_widget.setObjectName("windGusts_widget")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.windGusts_widget)
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.windSpeed_edit = QtWidgets.QSpinBox(self.windGusts_widget)
        self.windSpeed_edit.setMinimum(1)
        self.windSpeed_edit.setMaximum(50)
        self.windSpeed_edit.setProperty("value", 5)
        self.windSpeed_edit.setObjectName("windSpeed_edit")
        self.horizontalLayout_5.addWidget(self.windSpeed_edit)
        self.windGusts_enable = QtWidgets.QCheckBox(self.windGusts_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.windGusts_enable.sizePolicy().hasHeightForWidth())
        self.windGusts_enable.setSizePolicy(sizePolicy)
        self.windGusts_enable.setObjectName("windGusts_enable")
        self.horizontalLayout_5.addWidget(self.windGusts_enable)
        self.windGusts_edit = QtWidgets.QSpinBox(self.windGusts_widget)
        self.windGusts_edit.setEnabled(False)
        self.windGusts_edit.setMinimum(10)
        self.windGusts_edit.setProperty("value", 15)
        self.windGusts_edit.setObjectName("windGusts_edit")
        self.horizontalLayout_5.addWidget(self.windGusts_edit)
        self.gridLayout.addWidget(self.windGusts_widget, 4, 0, 1, 2)
        self.windHdg_radioButton = QtWidgets.QRadioButton(self.groupBox_4)
        self.windHdg_radioButton.setObjectName("windHdg_radioButton")
        self.gridLayout.addWidget(self.windHdg_radioButton, 1, 0, 1, 1)
        self.windVRB_radioButton = QtWidgets.QRadioButton(self.groupBox_4)
        self.windVRB_radioButton.setObjectName("windVRB_radioButton")
        self.gridLayout.addWidget(self.windVRB_radioButton, 2, 0, 1, 1)
        self.line = QtWidgets.QFrame(self.groupBox_4)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 3, 0, 1, 2)
        self.verticalLayout_4.addWidget(self.groupBox_4)
        self.groupBox = QtWidgets.QGroupBox(self.tab_2)
        self.groupBox.setObjectName("groupBox")
        self.formLayout = QtWidgets.QFormLayout(self.groupBox)
        self.formLayout.setObjectName("formLayout")
        self.label_4 = QtWidgets.QLabel(self.groupBox)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.visibility_edit = QtWidgets.QSpinBox(self.groupBox)
        self.visibility_edit.setMaximum(9900)
        self.visibility_edit.setSingleStep(500)
        self.visibility_edit.setObjectName("visibility_edit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.visibility_edit)
        self.label_7 = QtWidgets.QLabel(self.groupBox)
        self.label_7.setObjectName("label_7")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_7)
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.cloudLayer_select = QtWidgets.QComboBox(self.groupBox)
        self.cloudLayer_select.setObjectName("cloudLayer_select")
        self.cloudLayer_select.addItem("")
        self.cloudLayer_select.addItem("")
        self.cloudLayer_select.addItem("")
        self.cloudLayer_select.addItem("")
        self.cloudLayer_select.addItem("")
        self.horizontalLayout_10.addWidget(self.cloudLayer_select)
        self.cloudLayerHeight_edit = QtWidgets.QSpinBox(self.groupBox)
        self.cloudLayerHeight_edit.setMinimum(5)
        self.cloudLayerHeight_edit.setMaximum(300)
        self.cloudLayerHeight_edit.setSingleStep(5)
        self.cloudLayerHeight_edit.setProperty("value", 50)
        self.cloudLayerHeight_edit.setObjectName("cloudLayerHeight_edit")
        self.horizontalLayout_10.addWidget(self.cloudLayerHeight_edit)
        self.formLayout.setLayout(1, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_10)
        self.label_5 = QtWidgets.QLabel(self.groupBox)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.QNH_edit = QtWidgets.QSpinBox(self.groupBox)
        self.QNH_edit.setMinimum(980)
        self.QNH_edit.setMaximum(1039)
        self.QNH_edit.setProperty("value", 1013)
        self.QNH_edit.setObjectName("QNH_edit")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.QNH_edit)
        self.verticalLayout_4.addWidget(self.groupBox)
        self.applyWeather_button = QtWidgets.QPushButton(self.tab_2)
        self.applyWeather_button.setObjectName("applyWeather_button")
        self.verticalLayout_4.addWidget(self.applyWeather_button)
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.tab_4)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.ATCs_tableView = QtWidgets.QTableView(self.tab_4)
        self.ATCs_tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.ATCs_tableView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.ATCs_tableView.setObjectName("ATCs_tableView")
        self.ATCs_tableView.horizontalHeader().setStretchLastSection(True)
        self.ATCs_tableView.verticalHeader().setVisible(False)
        self.verticalLayout_7.addWidget(self.ATCs_tableView)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.addATC_button = QtWidgets.QToolButton(self.tab_4)
        self.addATC_button.setObjectName("addATC_button")
        self.horizontalLayout_6.addWidget(self.addATC_button)
        self.removeATC_button = QtWidgets.QToolButton(self.tab_4)
        self.removeATC_button.setObjectName("removeATC_button")
        self.horizontalLayout_6.addWidget(self.removeATC_button)
        self.verticalLayout_7.addLayout(self.horizontalLayout_6)
        self.tabWidget.addTab(self.tab_4, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.tab_3)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.situationSnapshots_tableView = QtWidgets.QTableView(self.tab_3)
        self.situationSnapshots_tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.situationSnapshots_tableView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.situationSnapshots_tableView.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.situationSnapshots_tableView.setObjectName("situationSnapshots_tableView")
        self.situationSnapshots_tableView.horizontalHeader().setStretchLastSection(True)
        self.situationSnapshots_tableView.verticalHeader().setVisible(False)
        self.verticalLayout_5.addWidget(self.situationSnapshots_tableView)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.snapshotSituation_button = QtWidgets.QToolButton(self.tab_3)
        self.snapshotSituation_button.setObjectName("snapshotSituation_button")
        self.horizontalLayout_3.addWidget(self.snapshotSituation_button)
        self.restoreSituation_button = QtWidgets.QToolButton(self.tab_3)
        self.restoreSituation_button.setObjectName("restoreSituation_button")
        self.horizontalLayout_3.addWidget(self.restoreSituation_button)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem4)
        self.removeSituation_button = QtWidgets.QToolButton(self.tab_3)
        self.removeSituation_button.setObjectName("removeSituation_button")
        self.horizontalLayout_3.addWidget(self.removeSituation_button)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.tabWidget.addTab(self.tab_3, "")
        self.gridLayout_3.addWidget(self.tabWidget, 0, 1, 1, 2)
        self.touchDownWithoutClearance_tickBox = QtWidgets.QCheckBox(teachingConsole)
        self.touchDownWithoutClearance_tickBox.setObjectName("touchDownWithoutClearance_tickBox")
        self.gridLayout_3.addWidget(self.touchDownWithoutClearance_tickBox, 1, 1, 1, 2)
        self.pauseSim_button = QtWidgets.QPushButton(teachingConsole)
        self.pauseSim_button.setCheckable(True)
        self.pauseSim_button.setObjectName("pauseSim_button")
        self.gridLayout_3.addWidget(self.pauseSim_button, 2, 1, 1, 1)
        self.skipFwd_button = QtWidgets.QPushButton(teachingConsole)
        self.skipFwd_button.setObjectName("skipFwd_button")
        self.gridLayout_3.addWidget(self.skipFwd_button, 2, 2, 1, 1)
        self.acftCallsign_info.setBuddy(self.spawn_button)
        self.label.setBuddy(self.xpdrMode_select)
        self.label_2.setBuddy(self.xpdrCode_select)
        self.label_3.setBuddy(self.xpdrIdent_tickBox)
        self.label_6.setBuddy(self.showDataLinkWindow_button)
        self.cpdlcStatus_info.setBuddy(self.showDataLinkWindow_button)
        self.label_8.setBuddy(self.cpdlcLogOn_button)
        self.label_4.setBuddy(self.visibility_edit)
        self.label_7.setBuddy(self.cloudLayer_select)
        self.label_5.setBuddy(self.QNH_edit)

        self.retranslateUi(teachingConsole)
        self.tabWidget.setCurrentIndex(0)
        self.windHdg_radioButton.toggled['bool'].connect(self.windDirection_widget.setEnabled) # type: ignore
        self.windGusts_enable.toggled['bool'].connect(self.windGusts_edit.setFocus) # type: ignore
        self.windHdgRange_enable.toggled['bool'].connect(self.windHdgRange_edit.setEnabled) # type: ignore
        self.windHdg_radioButton.toggled['bool'].connect(self.windHdg_dial.setFocus) # type: ignore
        self.windHdgRange_enable.toggled['bool'].connect(self.windHdgRange_edit.setFocus) # type: ignore
        self.windCalm_radioButton.toggled['bool'].connect(self.windGusts_widget.setDisabled) # type: ignore
        self.windGusts_enable.toggled['bool'].connect(self.windGusts_edit.setEnabled) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(teachingConsole)
        teachingConsole.setTabOrder(self.spawn_button, self.freezeACFT_button)
        teachingConsole.setTabOrder(self.freezeACFT_button, self.kill_button)
        teachingConsole.setTabOrder(self.kill_button, self.xpdrMode_select)
        teachingConsole.setTabOrder(self.xpdrMode_select, self.squat_tickBox)
        teachingConsole.setTabOrder(self.squat_tickBox, self.xpdrCode_select)
        teachingConsole.setTabOrder(self.xpdrCode_select, self.xpdrIdent_tickBox)
        teachingConsole.setTabOrder(self.xpdrIdent_tickBox, self.squawkVFR_button)
        teachingConsole.setTabOrder(self.squawkVFR_button, self.pushSQtoStrip_button)
        teachingConsole.setTabOrder(self.pushSQtoStrip_button, self.showDataLinkWindow_button)
        teachingConsole.setTabOrder(self.showDataLinkWindow_button, self.cpdlcLogOn_button)
        teachingConsole.setTabOrder(self.cpdlcLogOn_button, self.cpdlcTransfer_button)
        teachingConsole.setTabOrder(self.cpdlcTransfer_button, self.onTouchDown_land_radioButton)
        teachingConsole.setTabOrder(self.onTouchDown_land_radioButton, self.onTouchDown_touchAndGo_radioButton)
        teachingConsole.setTabOrder(self.onTouchDown_touchAndGo_radioButton, self.onTouchDown_skidOffRwy_radioButton)
        teachingConsole.setTabOrder(self.onTouchDown_skidOffRwy_radioButton, self.tabWidget)
        teachingConsole.setTabOrder(self.tabWidget, self.windCalm_radioButton)
        teachingConsole.setTabOrder(self.windCalm_radioButton, self.windHdg_radioButton)
        teachingConsole.setTabOrder(self.windHdg_radioButton, self.windVRB_radioButton)
        teachingConsole.setTabOrder(self.windVRB_radioButton, self.windHdg_dial)
        teachingConsole.setTabOrder(self.windHdg_dial, self.windHdgRange_enable)
        teachingConsole.setTabOrder(self.windHdgRange_enable, self.windHdgRange_edit)
        teachingConsole.setTabOrder(self.windHdgRange_edit, self.windSpeed_edit)
        teachingConsole.setTabOrder(self.windSpeed_edit, self.windGusts_enable)
        teachingConsole.setTabOrder(self.windGusts_enable, self.windGusts_edit)
        teachingConsole.setTabOrder(self.windGusts_edit, self.visibility_edit)
        teachingConsole.setTabOrder(self.visibility_edit, self.cloudLayer_select)
        teachingConsole.setTabOrder(self.cloudLayer_select, self.cloudLayerHeight_edit)
        teachingConsole.setTabOrder(self.cloudLayerHeight_edit, self.QNH_edit)
        teachingConsole.setTabOrder(self.QNH_edit, self.applyWeather_button)
        teachingConsole.setTabOrder(self.applyWeather_button, self.ATCs_tableView)
        teachingConsole.setTabOrder(self.ATCs_tableView, self.addATC_button)
        teachingConsole.setTabOrder(self.addATC_button, self.removeATC_button)
        teachingConsole.setTabOrder(self.removeATC_button, self.situationSnapshots_tableView)
        teachingConsole.setTabOrder(self.situationSnapshots_tableView, self.snapshotSituation_button)
        teachingConsole.setTabOrder(self.snapshotSituation_button, self.restoreSituation_button)
        teachingConsole.setTabOrder(self.restoreSituation_button, self.removeSituation_button)
        teachingConsole.setTabOrder(self.removeSituation_button, self.touchDownWithoutClearance_tickBox)

    def retranslateUi(self, teachingConsole):
        _translate = QtCore.QCoreApplication.translate
        teachingConsole.setWindowTitle(_translate("teachingConsole", "Teaching console"))
        self.selectedAircraft_box.setTitle(_translate("teachingConsole", "Selected ACFT"))
        self.acftCallsign_info.setText(_translate("teachingConsole", "###"))
        self.spawn_button.setText(_translate("teachingConsole", "Spawn"))
        self.freezeACFT_button.setText(_translate("teachingConsole", "Freeze"))
        self.kill_button.setText(_translate("teachingConsole", "Kill"))
        self.groupBox_2.setTitle(_translate("teachingConsole", "XPDR"))
        self.label.setText(_translate("teachingConsole", "Mode:"))
        self.xpdrMode_select.setItemText(0, _translate("teachingConsole", "Off"))
        self.xpdrMode_select.setItemText(1, _translate("teachingConsole", "A"))
        self.xpdrMode_select.setItemText(2, _translate("teachingConsole", "C"))
        self.xpdrMode_select.setItemText(3, _translate("teachingConsole", "C + S"))
        self.squat_tickBox.setToolTip(_translate("teachingConsole", "Sets GND bit automatically (mode S only)"))
        self.squat_tickBox.setText(_translate("teachingConsole", "Squats on ground"))
        self.label_2.setText(_translate("teachingConsole", "Code:"))
        self.label_3.setText(_translate("teachingConsole", "Options:"))
        self.xpdrIdent_tickBox.setText(_translate("teachingConsole", "Ident"))
        self.squawkVFR_button.setToolTip(_translate("teachingConsole", "Reset to uncontrolled VFR code"))
        self.squawkVFR_button.setText(_translate("teachingConsole", "Set VFR"))
        self.pushSQtoStrip_button.setToolTip(_translate("teachingConsole", "Write code as assignment on strip"))
        self.pushSQtoStrip_button.setText(_translate("teachingConsole", "Code to strip"))
        self.groupBox_5.setTitle(_translate("teachingConsole", "CPDLC"))
        self.label_6.setText(_translate("teachingConsole", "Data link status:"))
        self.cpdlcStatus_info.setText(_translate("teachingConsole", "###"))
        self.showDataLinkWindow_button.setToolTip(_translate("teachingConsole", "Open current/last CPDLC dialogue window for aircraft"))
        self.showDataLinkWindow_button.setText(_translate("teachingConsole", "Open"))
        self.label_8.setText(_translate("teachingConsole", "Connect to student:"))
        self.cpdlcLogOn_button.setText(_translate("teachingConsole", "ACFT log-on"))
        self.cpdlcTransfer_button.setToolTip(_translate("teachingConsole", "Simulate or abort a data authority transfer to the student"))
        self.cpdlcTransfer_button.setText(_translate("teachingConsole", "ATC transfer..."))
        self.onTouchDown_groupBox.setTitle(_translate("teachingConsole", "On touch-down"))
        self.onTouchDown_land_radioButton.setText(_translate("teachingConsole", "Land and vacate"))
        self.onTouchDown_touchAndGo_radioButton.setText(_translate("teachingConsole", "Touch and go"))
        self.onTouchDown_skidOffRwy_radioButton.setToolTip(_translate("teachingConsole", "Skid and crash off the runway"))
        self.onTouchDown_skidOffRwy_radioButton.setText(_translate("teachingConsole", "RWY excursion"))
        self.groupBox_4.setTitle(_translate("teachingConsole", "Wind"))
        self.windCalm_radioButton.setText(_translate("teachingConsole", "Calm"))
        self.windHdg_dial.setToolTip(_translate("teachingConsole", "Select blowing direction"))
        self.windHdgRange_enable.setText(_translate("teachingConsole", "±"))
        self.windHdgRange_edit.setSuffix(_translate("teachingConsole", "°"))
        self.windSpeed_edit.setSuffix(_translate("teachingConsole", " kt"))
        self.windGusts_enable.setText(_translate("teachingConsole", "gusts"))
        self.windGusts_edit.setSuffix(_translate("teachingConsole", " kt"))
        self.windHdg_radioButton.setText(_translate("teachingConsole", "###"))
        self.windVRB_radioButton.setText(_translate("teachingConsole", "VRB"))
        self.groupBox.setTitle(_translate("teachingConsole", "Other"))
        self.label_4.setText(_translate("teachingConsole", "Visibility:"))
        self.visibility_edit.setSpecialValueText(_translate("teachingConsole", "≥ 10 km"))
        self.visibility_edit.setSuffix(_translate("teachingConsole", " m"))
        self.label_7.setText(_translate("teachingConsole", "Cloud layer:"))
        self.cloudLayer_select.setItemText(0, _translate("teachingConsole", "None"))
        self.cloudLayer_select.setItemText(1, _translate("teachingConsole", "FEW"))
        self.cloudLayer_select.setItemText(2, _translate("teachingConsole", "SCT"))
        self.cloudLayer_select.setItemText(3, _translate("teachingConsole", "BKN"))
        self.cloudLayer_select.setItemText(4, _translate("teachingConsole", "OVC"))
        self.label_5.setText(_translate("teachingConsole", "QNH:"))
        self.QNH_edit.setSuffix(_translate("teachingConsole", " hPa"))
        self.applyWeather_button.setText(_translate("teachingConsole", "Apply weather"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("teachingConsole", "Weather"))
        self.addATC_button.setText(_translate("teachingConsole", "+"))
        self.removeATC_button.setText(_translate("teachingConsole", "–"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), _translate("teachingConsole", "ATCs"))
        self.snapshotSituation_button.setToolTip(_translate("teachingConsole", "Snapshot current situation"))
        self.snapshotSituation_button.setText(_translate("teachingConsole", "Snapshot current"))
        self.restoreSituation_button.setToolTip(_translate("teachingConsole", "Recall selected situation"))
        self.restoreSituation_button.setText(_translate("teachingConsole", "Recall selected"))
        self.removeSituation_button.setText(_translate("teachingConsole", "–"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("teachingConsole", "Situation snapshots"))
        self.touchDownWithoutClearance_tickBox.setText(_translate("teachingConsole", "All ACFT touch down without clearance"))
        self.pauseSim_button.setText(_translate("teachingConsole", "Pause simulation"))
        self.skipFwd_button.setText(_translate("teachingConsole", "Skip 10 s"))
from gui.widgets.miscWidgets import XpdrCodeSelectorWidget
