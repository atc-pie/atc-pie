# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'selectionInfoToolbarWidget.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_selectionInfoToolbarWidget(object):
    def setupUi(self, selectionInfoToolbarWidget):
        selectionInfoToolbarWidget.setObjectName("selectionInfoToolbarWidget")
        selectionInfoToolbarWidget.resize(382, 43)
        self.horizontalLayout = QtWidgets.QHBoxLayout(selectionInfoToolbarWidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.selection_info = QtWidgets.QLabel(selectionInfoToolbarWidget)
        self.selection_info.setObjectName("selection_info")
        self.horizontalLayout.addWidget(self.selection_info)
        self.flag_toggle = QtWidgets.QToolButton(selectionInfoToolbarWidget)
        self.flag_toggle.setCheckable(True)
        self.flag_toggle.setObjectName("flag_toggle")
        self.horizontalLayout.addWidget(self.flag_toggle)
        self.ignore_toggle = QtWidgets.QToolButton(selectionInfoToolbarWidget)
        self.ignore_toggle.setCheckable(True)
        self.ignore_toggle.setObjectName("ignore_toggle")
        self.horizontalLayout.addWidget(self.ignore_toggle)
        self.cheat_toggle = QtWidgets.QToolButton(selectionInfoToolbarWidget)
        self.cheat_toggle.setCheckable(True)
        self.cheat_toggle.setObjectName("cheat_toggle")
        self.horizontalLayout.addWidget(self.cheat_toggle)

        self.retranslateUi(selectionInfoToolbarWidget)
        QtCore.QMetaObject.connectSlotsByName(selectionInfoToolbarWidget)
        selectionInfoToolbarWidget.setTabOrder(self.flag_toggle, self.ignore_toggle)
        selectionInfoToolbarWidget.setTabOrder(self.ignore_toggle, self.cheat_toggle)

    def retranslateUi(self, selectionInfoToolbarWidget):
        _translate = QtCore.QCoreApplication.translate
        selectionInfoToolbarWidget.setWindowTitle(_translate("selectionInfoToolbarWidget", "Selection info"))
        self.selection_info.setText(_translate("selectionInfoToolbarWidget", "###"))
        self.flag_toggle.setToolTip(_translate("selectionInfoToolbarWidget", "Flag"))
        self.flag_toggle.setText(_translate("selectionInfoToolbarWidget", "F"))
        self.ignore_toggle.setToolTip(_translate("selectionInfoToolbarWidget", "Ignore"))
        self.ignore_toggle.setText(_translate("selectionInfoToolbarWidget", "I"))
        self.cheat_toggle.setToolTip(_translate("selectionInfoToolbarWidget", "Cheat"))
        self.cheat_toggle.setText(_translate("selectionInfoToolbarWidget", "C"))

