# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'cpdlcXfrOptionsDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_cpdlcXfrOptionsDialog(object):
    def setupUi(self, cpdlcXfrOptionsDialog):
        cpdlcXfrOptionsDialog.setObjectName("cpdlcXfrOptionsDialog")
        cpdlcXfrOptionsDialog.resize(569, 140)
        self.verticalLayout = QtWidgets.QVBoxLayout(cpdlcXfrOptionsDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(cpdlcXfrOptionsDialog)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.cpdlcTransfer_option = QtWidgets.QRadioButton(cpdlcXfrOptionsDialog)
        self.cpdlcTransfer_option.setChecked(True)
        self.cpdlcTransfer_option.setObjectName("cpdlcTransfer_option")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.SpanningRole, self.cpdlcTransfer_option)
        self.contactInstruction_option = QtWidgets.QRadioButton(cpdlcXfrOptionsDialog)
        self.contactInstruction_option.setObjectName("contactInstruction_option")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.contactInstruction_option)
        self.contactInstruction_info = QtWidgets.QLabel(cpdlcXfrOptionsDialog)
        self.contactInstruction_info.setObjectName("contactInstruction_info")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.contactInstruction_info)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(cpdlcXfrOptionsDialog)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(cpdlcXfrOptionsDialog)
        self.buttonBox.rejected.connect(cpdlcXfrOptionsDialog.reject)
        self.buttonBox.accepted.connect(cpdlcXfrOptionsDialog.accept)
        QtCore.QMetaObject.connectSlotsByName(cpdlcXfrOptionsDialog)

    def retranslateUi(self, cpdlcXfrOptionsDialog):
        _translate = QtCore.QCoreApplication.translate
        cpdlcXfrOptionsDialog.setWindowTitle(_translate("cpdlcXfrOptionsDialog", "CPDLC action on strip drop"))
        self.label.setText(_translate("cpdlcXfrOptionsDialog", "Select CPDLC action to perform:"))
        self.cpdlcTransfer_option.setText(_translate("cpdlcXfrOptionsDialog", "Transfer data authority"))
        self.contactInstruction_option.setText(_translate("cpdlcXfrOptionsDialog", "Instruct through CPDLC:"))
        self.contactInstruction_info.setText(_translate("cpdlcXfrOptionsDialog", "###"))
