# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'fgcomProcessDialog.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_fgcomProcessDialog(object):
    def setupUi(self, fgcomProcessDialog):
        fgcomProcessDialog.setObjectName("fgcomProcessDialog")
        fgcomProcessDialog.resize(253, 83)
        self.verticalLayout = QtWidgets.QVBoxLayout(fgcomProcessDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.info_label = QtWidgets.QLabel(fgcomProcessDialog)
        self.info_label.setObjectName("info_label")
        self.verticalLayout.addWidget(self.info_label)
        self.close_button = QtWidgets.QPushButton(fgcomProcessDialog)
        self.close_button.setObjectName("close_button")
        self.verticalLayout.addWidget(self.close_button)

        self.retranslateUi(fgcomProcessDialog)
        QtCore.QMetaObject.connectSlotsByName(fgcomProcessDialog)

    def retranslateUi(self, fgcomProcessDialog):
        _translate = QtCore.QCoreApplication.translate
        self.info_label.setText(_translate("fgcomProcessDialog", "###"))
        self.close_button.setText(_translate("fgcomProcessDialog", "Close"))

