
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from sys import stderr
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_REUSEADDR

from PyQt5.QtCore import QMutex
from PyQt5.QtWidgets import QMessageBox

from base.acft import Xpdr
from base.coords import EarthCoords
from base.cpdlc import CPDLC_element_display_text
from base.fpl import FPL
from base.params import PressureAlt, Speed
from base.radio import CommFrequency
from base.strip import Strip, handover_details, received_from_detail
from base.text import ChatMessage
from base.util import pop_all
from base.weather import Weather

from ext.fgcom import FGComRadio, FGCom_tick_interval, send_FGCom_mumble_control_packet, receive_FGCom_mumble_packet, record_ATIS_dialog_exec
from ext.fgfs import send_packet_to_views
from ext.fsd import FsdConnection, FsdAircraft, FPL_from_fields
from ext.hoppie import HoppieCommunicator
from ext.mumble import MumbleCommunicator, mumble_available
from ext.noaa import RealWeatherChecker

from gui.actions import register_weather_information
from gui.misc import signals
from gui.widgets.basicWidgets import Ticker

from session.config import settings, version_string
from session.env import env
from session.manager import SessionManager, SessionType, TextMsgBlocked, HandoverBlocked, CpdlcOperationBlocked, OnlineFplActionBlocked
from session.managers.flightGearMP import UdpSessionListener


# ---------- Constants ----------

position_update_interval = 5000 # ms
ATIS_ticker_interval = 15000 # ms
TM_escape_prefix = '___ATC-pie___'
RN_ACFT_type_separator = '//' # for pilots to declare their ACFT type in "real name" field
missing_client_type_str = '!!type'

# -------------------------------


# ATC-pie escaped commands in "#TM" packets:
#  - ATCPIE   Declare ATC-pie version and social name
#  - STRIP    Strip exchange
#  - WHOHAS   Who-has request
#  - IHAVE    Who-has answer
#  - CPDLC_XFR_INIT    Data link transfer proposal
#  - CPDLC_XFR_CANCEL  Data link transfer proposal cancelled
#  - CPDLC_XFR_ACCEPT  Data link transfer accept
#  - CPDLC_XFR_REJECT  Data link transfer reject



class ClientInfoKey:
	info_keys = CID, SOCIAL_NAME, IS_ATCPIE, TYPE = range(4)





def dest_concerns_me(dest):
	# '@'-prefixed freq's are 5-digit strings containing value in kHz without the leading '1'
	if dest.startswith('@') and dest[1:].isdigit() and settings.publicised_frequency is not None:
		return CommFrequency('1' + dest[1:]).inTune(settings.publicised_frequency)
	else:
		return dest in ['*', '*A', '@499999', settings.my_callsign] # @499999 seems to be Euroscope's "all ATCs"





class FsdSessionManager(SessionManager):
	def __init__(self, gui):
		SessionManager.__init__(self, gui, SessionType.FSD)
		self.socket = None # None here when simulation NOT running
		self.disconnect_on_prupose = False
		self.last_received_error = None
		self.client_table = {} # (callsign, info key) -> value
		self.ACFT_list = [] # FsdAircraft list
		self.ACFT_list_mutex = QMutex() # Possibly critical: FSD connection modifying traffic vs. getAircraft
		self.FSD_connection = FsdConnection(gui)
		self.position_update_ticker = Ticker(gui, self.FSD_connection.sendPositionUpdate)
		self.weather_ticker = Ticker(gui, self.weatherTick)
		self.ATIS_ticker = Ticker(gui, self.atisTick)
		self.real_weather_checker = None if settings.FSD_weather_from_server else RealWeatherChecker(gui, register_weather_information)
		self.Hoppie_communicator = HoppieCommunicator(gui) if settings.FSD_Hoppie_enabled else None
		self.FSD_connection.cmdReceived.connect(self.fsdCmdReceived)
		self.FSD_connection.connectionDropped.connect(self._fsdDisconnected)
		self.Mumble_communicator = MumbleCommunicator(gui) if mumble_available and settings.phone_lines_enabled else None
	
	def _fsdDisconnected(self):
		self.position_update_ticker.stop()
		self.weather_ticker.stop()
		self.ATIS_ticker.stop()
		signals.fastClockTick.disconnect(self.updateAllAcftLiveStatuses)
		if settings.FSD_FGCom_enabled:
			self.fgcom_ticker.stop()
			self.fgcom_listener.stop()
			self.fgcom_listener.wait()
			self.fgcom_socket = None
		if self.Mumble_communicator is not None:
			self.Mumble_communicator.stopAndWait()
		if self.Hoppie_communicator is not None:
			self.Hoppie_communicator.stopPolling()
		if self.real_weather_checker is not None:
			self.real_weather_checker.wait()
		self.ACFT_list.clear()
		self.client_table.clear()
		signals.sessionEnded.emit(SessionType.FSD)
		if not self.disconnect_on_prupose:
			msg = 'Connection dropped.'
			if self.last_received_error is not None:
				msg += '\nLast error message received: "%s"' % self.last_received_error
			QMessageBox.critical(self.gui, 'FSD error', msg)
	
	def start(self):
		self.disconnect_on_prupose = False
		self.last_received_error = None
		if self.FSD_connection.initOK():
			print('Connected to FSD server.')
			self.ACFT_list.clear()
			self.client_table.clear()
			if settings.FSD_FGCom_enabled:
				try:
					self.fgcom_socket = socket(AF_INET, SOCK_DGRAM)
					self.fgcom_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
					self.fgcom_socket.bind(('', settings.FSD_FGCom_port))
					self.fgcom_ticker = Ticker(self.gui, lambda: send_FGCom_mumble_control_packet(self.fgcom_socket, settings.FGCom_mumble_host, settings.FGCom_mumble_port, settings.FGCom_mumble_sound_effects))
					self.fgcom_ticker.start(FGCom_tick_interval)
					self.fgcom_listener = UdpSessionListener(self.gui, self.fgcom_socket, receive_FGCom_mumble_packet)
					self.fgcom_listener.start()
				except OSError as error:
					self.socket = None
					print('FGCom socket creation error: %s' % error, file=stderr)
					return
			if self.Mumble_communicator is not None:
				self.Mumble_communicator.start()
			if self.Hoppie_communicator is not None:
				self.Hoppie_communicator.startPolling()
			signals.sessionStarted.emit(SessionType.FSD)
			signals.fastClockTick.connect(self.updateAllAcftLiveStatuses)
			if settings.FSD_METAR_update_interval is not None:
				self.weather_ticker.startTicking(settings.FSD_METAR_update_interval)
				self.ATIS_ticker.startTicking(ATIS_ticker_interval)
			self.position_update_ticker.startTicking(position_update_interval)
			self.sendAtcPieEscapedMsg('ATCPIE', version_string) # to all ATCs
		else:
			QMessageBox.critical(self.gui, 'FSD error', 'Connection failed.')
	
	def stop(self):
		if self.isRunning():
			self.disconnect_on_prupose = True
			self.FSD_connection.shutdown() # emits a disconnection signal
	
	def isRunning(self):
		return self.FSD_connection.isConnected()
	
	def getAircraft(self):
		self.ACFT_list_mutex.lock()
		result = self.ACFT_list[:]
		self.ACFT_list_mutex.unlock()
		return result
	
	
	## ACFT/ATC INTERACTION
	
	def instructAircraftByCallsign(self, callsign, instr):
		signals.chatInstructionSuggestion.emit(callsign, instr.readOutStr(env.radarContactByCallsign(callsign)))
	
	def postRadioChatMsg(self, msg):
		if settings.publicised_frequency is None:
			raise TextMsgBlocked('No publicised frequency to post radio message to.')
		self.FSD_connection.sendTextMsg(msg, frq=settings.publicised_frequency)
	
	def postAtcChatMsg(self, msg):
		if msg.txtOnly().startswith(TM_escape_prefix):
			raise TextMsgBlocked('Message starts with "%s".' % TM_escape_prefix)
		self.FSD_connection.sendTextMsg(msg)
	
	def sendStrip(self, strip, atc):
		if self.client_table.get((atc, ClientInfoKey.IS_ATCPIE), False):
			self.sendAtcPieEscapedMsg('STRIP', strip.encodeDetails(handover_details), privateTo=atc)
		else: # sending to a *NON* ATC-pie client
			cs = strip.callsign() # XPDR not normally squawking callsign anyway
			if cs is None:
				raise HandoverBlocked('A callsign must be on the strip when not sending to ATC-pie.')
			QMessageBox.warning(self.gui, 'Hand-over warning', 'This ATC is not using ATC-pie. '
					'Handover will only be notified if callsign is connected to the server, '
					'and all strip changes and assignments will be lost (recipient will likely rely on FPL).')
			#TODO above, once amending online FPLs becomes possible:
			#if fpl is None or not fpl.isOnline() or fpl.hasLocalChanges() or strip.fplConflicts():
			#	offer to push strip details, update FPL... to mitigate lossy $HO (comparable to "lossy shelving" safeguard)
			self.FSD_connection.sendNonAtcPieHandover(atc, cs)
	
	def sendWhoHas(self, callsign):
		self.sendAtcPieEscapedMsg('WHOHAS', callsign) # to all ATCs
	
	def sendCpdlcMsg(self, callsign, msg):
		if self.Hoppie_communicator is None:
			raise CpdlcOperationBlocked('Hoppie sub-system must be enabled for CPDLC in FSD sessions.')
		encoded = '/'.join(CPDLC_element_display_text(elt.replace('@', '_'), varFmt='@%s@') for elt in msg.elements())
		ra_lst = ['WU', 'AN', 'R', 'Y', 'N'] if msg.isUplink() else ['Y', 'N']
		self.Hoppie_communicator.sendCpdlcData(callsign, encoded, ra_lst[msg.responseAttributePrecedence() - 1], incrPolling=msg.expectsAnswer())

	def sendCpdlcTransferRequest(self, acft_callsign, atc_callsign, proposing):
		if self.Hoppie_communicator is None:
			raise CpdlcOperationBlocked('Hoppie sub-system must be enabled for CPDLC in FSD sessions.')
		if proposing:
			self.sendAtcPieEscapedMsg('CPDLC_XFR_INIT', acft_callsign, privateTo=atc_callsign)
			self.Hoppie_communicator.sendCpdlcData(acft_callsign, 'HANDOVER @%s' % atc_callsign, 'NE', incrPolling=True) # e.g. /data2/49//NE/HANDOVER @EDYR
		else:
			self.sendAtcPieEscapedMsg('CPDLC_XFR_CANCEL', acft_callsign, privateTo=atc_callsign)

	def sendCpdlcTransferResponse(self, acft_callsign, atc_callsign, accepting):
		if self.Hoppie_communicator is None:
			raise CpdlcOperationBlocked('Hoppie sub-system must be enabled for CPDLC in FSD sessions.')
		if accepting:
			self.sendAtcPieEscapedMsg('CPDLC_XFR_ACCEPT', acft_callsign, privateTo=atc_callsign)
			self.Hoppie_communicator.sendCpdlcData(acft_callsign, 'LOGON ACCEPTED', 'NE')
		else:
			self.sendAtcPieEscapedMsg('CPDLC_XFR_REJECT', acft_callsign, privateTo=atc_callsign)

	def disconnectCpdlc(self, callsign):
		if self.Hoppie_communicator is None:
			raise CpdlcOperationBlocked('Hoppie sub-system must be enabled for CPDLC in FSD sessions.')
		self.Hoppie_communicator.sendCpdlcData(callsign, 'LOGOFF', 'NE')
	
	
	## VOICE COMM'S
	
	def createRadio(self):
		if settings.FSD_FGCom_enabled:
			return FGComRadio()
		else:
			QMessageBox.information(self.gui, 'Create radio', 'FGCom-mumble sub-system must be enabled for integrated radios.')
	
	def recordAtis(self, parent_dialog):
		if settings.FSD_FGCom_enabled:
			record_ATIS_dialog_exec(parent_dialog)
		else:
			QMessageBox.information(parent_dialog, 'Record ATIS', 'FGCom-mumble not enabled; ATIS recorded as text only.')
	
	def landLineManager(self):
		return self.Mumble_communicator # can be None
	
	
	## ONLINE SYSTEMS
	
	def weatherLookUpRequest(self, station):
		if settings.FSD_weather_from_server:
			self.FSD_connection.sendMetarRequest(station)
		else:
			self.real_weather_checker.lookupStation(station)
	
	def pushFplOnline(self, fpl):
		raise OnlineFplActionBlocked('FSD does not allow ATCs to file or amend FPLs online.')
		#if fpl.isOnline(): # pushing local changes to an ONLINE flight plan
		#	if FPL.CALLSIGN in fpl.modified_details:
		#		QMessageBox.critical(self.gui, 'FPL update error',
		#				'FSD does not allow to edit FPL callsigns.\nPlease revert.')
		#	else:
		#		self.FSD_connection.sendFplAmendment(fpl)
		#		for d in list(fpl.modified_details):
		#			if d not in [FPL.CALLSIGN, FPL.WTC, FPL.SOULS]:
		#				del fpl.modified_details[d]
		#else: # pushing a LOCAL flight plan online
		#	cs = fpl[FPL.CALLSIGN]
		#	if not self.knownClient(cs) or cs in env.ATCs.knownAtcCallsigns():
		#		QMessageBox.critical(self.gui, 'FPL upload error', 'FSD only allows to file FPLs for connected pilots.')
		#		return
		#	got_online = env.FPLs.findAll(lambda fpl: fpl.online_id == cs)
		#	if len(got_online) == 0:
		#		self.FSD_connection.sendNewFpl(fpl)
		#		fpl.markAsOnline(cs)
		# else: # got FPL online with same callsign
		#		QMessageBox.critical(self.gui, 'FPL upload error',
		#				'FSD only allows one FPL online per callsign at a time.\nPlease edit the online one instead.')
		#		signals.fplEditRequest.emit(got_online)
	
	def changeFplStatus(self, fpl, new_status):
		raise OnlineFplActionBlocked('FSD does not implement FPL open/close.')
	
	def syncOnlineFPLs(self):
		for callsign in set(cs for cs, inf in self.client_table):
			if callsign not in env.ATCs.knownAtcCallsigns():
				self.FSD_connection.sendQuery('SERVER', 'FP:%s' % callsign)
	
	
	## MANAGER-SPECIFIC
	
	def registerClientInfo(self, callsign, info_key, value, reqFplIfNew=True):
		if reqFplIfNew and not any(cs == callsign for cs, inf in self.client_table):
			self.FSD_connection.sendQuery('SERVER', 'FP:%s' % callsign)
		self.client_table[callsign, info_key] = value
	
	def updateAllAcftLiveStatuses(self):
		self.ACFT_list_mutex.lock()
		for fsd_acft in self.ACFT_list:
			fsd_acft.updateLiveStatusWithEstimate()
			send_packet_to_views(fsd_acft.fgmsLivePositionPacket())
		self.ACFT_list_mutex.unlock()
	
	def weatherTick(self):
		if settings.FSD_weather_from_server:
			self.FSD_connection.sendMetarRequest(settings.primary_METAR_station)
			for station in settings.additional_METAR_stations:
				self.FSD_connection.sendMetarRequest(station)
		else:
			self.real_weather_checker.lookupSelectedStations()
	
	def atisTick(self):
		if settings.last_recorded_ATIS is not None:
			for line in settings.last_recorded_ATIS[3].split('\n'):
				if line.strip() != '': # avoid empty lines in text chat
					self.FSD_connection.sendTextMsg(ChatMessage(settings.my_callsign, line), frq=settings.last_recorded_ATIS[2])

	def sendAtcPieEscapedMsg(self, escaped_cmd, arg_str, privateTo=None):
		msg_txt = '%s%s %s' % (TM_escape_prefix, escaped_cmd, arg_str)
		if privateTo is None:
			msg = ChatMessage(settings.my_callsign, msg_txt, private=False)
		else:
			msg = ChatMessage(settings.my_callsign, msg_txt, recipient=privateTo, private=True)
		self.FSD_connection.sendTextMsg(msg)
	
	def processEscapedTM(self, sender, cmd, arg):
		if cmd == 'ATCPIE': # arg: version
			if not self.client_table.get((sender, ClientInfoKey.IS_ATCPIE), False):
				self.registerClientInfo(sender, ClientInfoKey.IS_ATCPIE, True, reqFplIfNew=False)
				self.sendAtcPieEscapedMsg('ATCPIE', version_string, privateTo=sender)
		elif cmd == 'STRIP': # arg: encoded strip details
			strip = Strip.fromEncodedDetails(arg)
			strip.writeDetail(received_from_detail, sender)
			signals.receiveStrip.emit(strip)
		elif cmd == 'WHOHAS': # arg: callsign
			if env.shouldAnswerWhoHas(arg):
				self.sendAtcPieEscapedMsg('IHAVE', arg, privateTo=sender)
		elif cmd == 'IHAVE': # arg: callsign
			signals.incomingContactClaim.emit(sender, arg)
		elif cmd == 'CPDLC_XFR_INIT': # arg: ACFT callsign
			signals.cpdlcTransferRequest.emit(arg, sender, True)
		elif cmd == 'CPDLC_XFR_CANCEL': # arg: ACFT callsign
			signals.cpdlcTransferRequest.emit(arg, sender, False)
		elif cmd == 'CPDLC_XFR_ACCEPT': # arg: ACFT callsign
			signals.cpdlcTransferResponse.emit(arg, sender, True)
		elif cmd == 'CPDLC_XFR_REJECT': # arg: ACFT callsign
			signals.cpdlcTransferResponse.emit(arg, sender, False)
		else:
			print('Unhandled "%s" message from %s with arg "%s"' % (cmd, sender, arg), file=stderr)
	
	def fsdCmdReceived(self, cmd, fields):
		if cmd == '#AA' and len(fields) == 6: # Add ATC client
			# example data: LFPG:SERVER:Michael:100997::2
			callsign, srv, social_name, cid, wtf, rating = fields
			self.registerClientInfo(callsign, ClientInfoKey.CID, cid, reqFplIfNew=False) # ensure at least one info key in for callsign
			self.registerClientInfo(callsign, ClientInfoKey.SOCIAL_NAME, social_name, reqFplIfNew=False)
		
		elif cmd == '#AP' and len(fields) == 7: # Add pilot client
			# example data: ATCFSX:SERVER:222222::1:1:9
			callsign, srv, cid, wtf1, wtf2, wtf3, wtf4 = fields
			self.registerClientInfo(callsign, ClientInfoKey.CID, cid) # ensure at least one info key in for callsign
		
		elif cmd == '#DA' and len(fields) == 2: # Remove ATC client
			# example data: LFPG:100997
			callsign, cid = fields
			for k in list(self.client_table): # detached list of keys
				if k[0] == callsign:
					del self.client_table[k]
			try:
				env.ATCs.removeATC(callsign)
			except KeyError: # "#AA" not received if this ATC connected before us
				pass
		
		elif cmd == '#DL' and len(fields) == 4: # Heart beat from server
			# example data: SERVER:*:-7:0
			pass
		
		elif cmd == '#DP' and len(fields) == 2: # Remove pilot client
			# example data: ATCFSX:222222
			callsign, cid = fields
			for k in list(self.client_table): # detached list of keys
				if k[0] == callsign:
					del self.client_table[k]
			self.ACFT_list_mutex.lock()
			pop_all(self.ACFT_list, lambda acft: acft.identifier == callsign)
			self.ACFT_list_mutex.unlock()
		
		elif cmd == '#TM' and len(fields) == 3: # Text message
			# example data: server:LFPN:FSFDT Windows FSD Beta from FSD V3.000 draft 9
			src, dest_conjunction, msg = fields
			for dest in dest_conjunction.split('&'): # such conjunctions were observed, at least coming from Euroscope
				if dest_concerns_me(dest):
					if src == 'server': # Bypass integrated comm systems (radio and ATC) if message is from server
						signals.statusBarMsg.emit('Server message: ' + msg)
						print('FSD server sends message:', msg)
					elif dest.startswith('@'): # Radio chat message
						signals.incomingRadioChatMsg.emit(ChatMessage(src, msg, private=False))
					elif msg.startswith(TM_escape_prefix): # ATC-pie escaped message
						split = msg[len(TM_escape_prefix):].split(' ', maxsplit=1)
						self.processEscapedTM(src, split[0], ('' if len(split) == 1 else split[1]))
					elif dest == settings.my_callsign: # Private text chat message
						signals.incomingAtcTextMsg.emit(ChatMessage(src, msg, recipient=settings.my_callsign, private=True))
					else: # Public ATC or broadcast text chat message
						signals.incomingAtcTextMsg.emit(ChatMessage(src, msg, recipient=None, private=False))
		
		elif cmd == '$AR' and len(fields) == 4:
			# example data: server:IGNORE_ME:METAR:EICK 182000Z 08012KT 0600 R16/P2000 R34/P2000 -DZ FG OVC001 08/08 Q1017 NOSIG
			if fields[2] == 'METAR':
				register_weather_information(Weather(fields[3]))
			else:
				print('Unhandled $AR "%s" packet' % fields[2], file=stderr)
		
		elif cmd == '$CQ' and len(fields) == 3: # Query
			# example data: PHCO:EHAM:ATIS
			src, dest, req = fields
			if dest_concerns_me(dest):
				if req == 'RN': # real name
					self.FSD_connection.sendQueryResponse(src, 'RN', '%s::%d' % (settings.MP_social_name, settings.FSD_rating))
		
		elif cmd == '$CR' and len(fields) == 4: # Query response
			# example data: PHJDW:IGNORE_ME:RN:Jan de Wit EHLE::1
			src, dest, req, answer = fields
			if dest_concerns_me(dest):
				if req == 'RN': # real name
					real_name_field = answer.split(':', maxsplit=1)[0]
					if RN_ACFT_type_separator in real_name_field:
						lft, rgt = real_name_field.split(RN_ACFT_type_separator, maxsplit=1)
						self.registerClientInfo(src, ClientInfoKey.TYPE, lft)
						self.registerClientInfo(src, ClientInfoKey.SOCIAL_NAME, rgt)
					else:
						self.registerClientInfo(src, ClientInfoKey.TYPE, missing_client_type_str)
						self.registerClientInfo(src, ClientInfoKey.SOCIAL_NAME, real_name_field)
				else:
					print('Unexpected $CR response "%s" from %s' % (req, src), file=stderr)
		
		elif cmd == '$ER' and len(fields) == 5: # Error message
			# example data: server:LFPN:004::Syntax error
			src, dest, errcode, wtf, msg = fields
			self.last_received_error = msg
			if not (errcode.isdigit() and int(errcode) == 8): # "no FPL" is not to be reported
				print('FSD error packet received:', ':'.join(fields), file=stderr)
		
		elif cmd == '$FP' and len(fields) == 17: # Flight plan
			# example data: KLM002:*A:V:B738:100:EHAM:1226:0:FL070:EGLL:0:0:0:0::/V/:XAMAN
			fpl = FPL_from_fields(*fields)
			fpl.markAsOnline(fields[0]) # FSD only allows one FPL per callsign at a time, so we use callsign as online ID
			env.FPLs.updateFromOnlineDownload(fpl)
		
		elif cmd == '$HO' and len(fields) == 3: # Handover
			# example data: EHAM_APP:EHAM_TST:ATCFSX
			src, dest, acft_callsign = fields
			strip = Strip()
			strip.writeDetail(FPL.CALLSIGN, acft_callsign)
			got_online = env.FPLs.findAll(lambda fpl: fpl.online_id == acft_callsign)
			if len(got_online) == 0:
				print('Received strip from non ATC-pie client while no FPL was found online for %s' % acft_callsign, file=stderr)
			else:
				strip.fillFromFPL(useFpl=got_online)
			strip.writeDetail(received_from_detail, src)
			signals.receiveStrip.emit(strip)
		
		elif cmd == '%' and len(fields) == 8: # ATC update
			# example data: LFPG:0:5:720:2:49.00975:2.54786:0
			callsign, frq, wtf1, vis, rating, lat, lon, wtf2 = fields
			try:
				social_name = self.client_table[callsign, ClientInfoKey.SOCIAL_NAME]
			except KeyError: # "#AA" not received if this ATC connected before us
				social_name = None
				self.FSD_connection.sendQuery(callsign, 'RN')
			try:
				coords = EarthCoords(float(lat), float(lon))
				comm_freq = None if frq == '' or frq == '0' else CommFrequency('1' + frq)
			except ValueError as err:
				print('Value error in position update from "%s": %s' % (callsign, err), file=stderr)
			else:
				env.ATCs.updateATC(callsign, coords, social_name, comm_freq) # adds or updates
		
		elif cmd == '@' and len(fields) == 10: # Pilot update
			# example data: @N:KLM002:2000:1:52.30477:4.77602:-3:0:4196062:406
			sqmode, callsign, sqcode, rating, lat, lon, amsl, spd, wtf1, wtf2 = fields
			try:
				coords = EarthCoords(float(lat), float(lon))
				real_alt = float(amsl)
				gnd_speed = Speed(float(spd))
				xpdr = {}
				if sqmode != 'S': # not "standby"
					# NOTE: FSD does not support mode S, so the following data is always missing: CALLSIGN, ACFT, GND, IAS, MACH
					xpdr[Xpdr.CODE] = int(sqcode, base=8)
					xpdr[Xpdr.IDENT] = sqmode == 'Y' # otherwise presumably 'N'
					xpdr[Xpdr.ALT] = PressureAlt(real_alt) # NOTE: no pressure-alt. available; equating with real alt.
			except ValueError as err:
				print('Value error in position update from "%s": %s' % (callsign, err), file=stderr)
			else:
				self.ACFT_list_mutex.lock()
				try:
					fsd_acft = next(acft for acft in self.ACFT_list if acft.identifier == callsign)
					fsd_acft.updatePdStatus(coords, real_alt, gnd_speed, xpdr)
				except StopIteration: # new callsign
					try:
						fsd_acft = FsdAircraft(callsign, self.client_table[callsign, ClientInfoKey.TYPE], coords, real_alt, gnd_speed, xpdr)
						self.ACFT_list.append(fsd_acft)
					except KeyError: # "#AP" not received if pilot connected first
						self.FSD_connection.sendQuery(callsign, 'RN') # this is to retrieve a type for the aircraft
						fsd_acft = None
				self.ACFT_list_mutex.unlock()
				if fsd_acft is not None:
					send_packet_to_views(fsd_acft.fgmsLivePositionPacket())
		
		else:
			print('Ignoring unhandled "%s" packet.' % cmd, file=stderr)
