
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from sys import stderr
from datetime import timedelta
try:
	from pyaudio import PyAudio, paInt16
	pyaudio_available = True
except ImportError:
	pyaudio_available = False

from PyQt5.QtCore import pyqtSignal, QObject, QThread
from PyQt5.QtNetwork import QTcpServer
from PyQt5.QtWidgets import QMessageBox

from ai.controlledAircraft import ControlledAiAircraft

from base.util import pop_all
from base.fpl import FPL
from base.db import wake_turb_cat
from base.text import ChatMessage
from base.cpdlc import CpdlcMessage
from base.phone import RMS_amplitude, AbstractLandLineManager
from base.strip import Strip, handover_details, received_from_detail, sent_to_detail

from session.config import settings
from session.env import env
from session.manager import SessionManager, SessionType, student_callsign, teacher_callsign, TextMsgBlocked, HandoverBlocked, CpdlcOperationBlocked

from ext.fgfs import send_packet_to_views

from gui.misc import selection, signals
from gui.actions import register_weather_information
from gui.dialog.createTraffic import CreateTrafficDialog
from gui.widgets.basicWidgets import Ticker


# ---------- Constants ----------

teacher_ticker_interval = 200 # ms
max_noACK_traffic = 20

CPDLC_cmd_prefix_conn = 'CX:' # connection/disconnection
CPDLC_cmd_prefix_xfr = 'XFR:' # transfers (proposed, cancelled, accepted, rejected)
CPDLC_cmd_prefix_msg = 'MSG:' # regular dialogue message

phone_audio_chunk_size = 1024
phone_audio_sample_rate = 16000

# -------------------------------



class TeachingMsg:
	msg_types = SIM_PAUSED, SIM_RESUMED, TIME_SKIP, ACFT_KILLED, ATC_TEXT_CHAT, \
							STRIP_EXCHANGE, FLIGHT_PLAN, ATC_LIST, WEATHER, TRAFFIC, CPDLC, \
							RADIO_PTT, RADIO_AUDIO, PHONE_REQUEST, PHONE_AUDIO = range(15)
	
	def __init__(self, msg_type, data=None):
		self.type = msg_type
		self.data = b''
		if data is not None:
			self.appendData(data)
	
	def appendData(self, data):
		self.data += data if isinstance(data, bytes) else data.encode('utf8')
	
	def binData(self):
		return self.data
	
	def strData(self):
		return self.data.decode('utf8')






class TeachingSessionWire(QObject):
	messageArrived = pyqtSignal(TeachingMsg)
	
	def __init__(self, socket):
		QObject.__init__(self)
		self.socket = socket
		self.got_msg_type = None
		self.got_data_len = None
		self.socket.readyRead.connect(self.readAvailableBytes)

	def readAvailableBytes(self):
		if self.got_msg_type is None:
			if self.socket.bytesAvailable() < 1:
				return
			self.got_msg_type = int.from_bytes(self.socket.read(1), 'big')
		if self.got_data_len is None:
			if self.socket.bytesAvailable() < 4:
				return
			self.got_data_len = int.from_bytes(self.socket.read(4), 'big')
		if self.socket.bytesAvailable() < self.got_data_len:
			return
		self.messageArrived.emit(TeachingMsg(self.got_msg_type, data=self.socket.read(self.got_data_len)))
		self.got_msg_type = self.got_data_len = None
		if self.socket.bytesAvailable() > 0:
			self.socket.readyRead.emit()
	
	def sendMessage(self, msg):
		#DEBUG if msg.type != TeachingMsg.TRAFFIC:
		#DEBUG 	print('Sending: %s' % msg.data)
		buf = msg.type.to_bytes(1, 'big') # message type code
		buf += len(msg.data).to_bytes(4, 'big') # length of data
		buf += msg.data # message data
		self.socket.write(buf)




class TeachingPhoneRadioManager(AbstractLandLineManager, QThread):
	messageToSend = pyqtSignal(TeachingMsg)

	def __init__(self, parent):
		AbstractLandLineManager.__init__(self)
		QThread.__init__(self, parent)
		self.py_audio = PyAudio()
		self.running = False
		self.call_in_progress = None
		self.radio_PTT = False

	def run(self):
		self.audio_in = self.py_audio.open(format=paInt16, channels=1, rate=phone_audio_sample_rate,
				frames_per_buffer=phone_audio_chunk_size, input=True, output=True, start=False)
		self.audio_out = self.py_audio.open(format=paInt16, channels=1, rate=phone_audio_sample_rate,
				frames_per_buffer=phone_audio_chunk_size, output=True)
		for atc in env.ATCs.knownAtcCallsigns():
			if atc != student_callsign and atc != teacher_callsign:
				self.createLandLine(atc)
		signals.newATC.connect(self.createLandLine)
		signals.kbdPTT.connect(self.setRadioPTT)
		self.running = True
		while self.running:
			if not self.radioPTT() and self.call_in_progress is None: # there is no reason to keep recording
				if not self.audio_in.is_stopped():
					self.audio_in.stop_stream()
				self.msleep(100)
			else: # a phone call or radio message is in progress
				if self.audio_in.is_stopped():
					self.audio_in.start_stream()
				sound_data = self.audio_in.read(phone_audio_chunk_size, exception_on_overflow=False)
				if self.radioPTT():
					self.messageToSend.emit(TeachingMsg(TeachingMsg.RADIO_AUDIO, data=sound_data))
				elif RMS_amplitude(sound_data) > settings.phone_line_squelch:
					self.messageToSend.emit(TeachingMsg(TeachingMsg.PHONE_AUDIO, data=sound_data))
		self.audio_in.close()
		self.audio_out.close()

	def stopAndWait(self):
		self.running = False
		self.radio_PTT = False
		self.call_in_progress = None
		signals.kbdPTT.disconnect(self.setRadioPTT)
		signals.newATC.disconnect(self.createLandLine)
		for atc in env.ATCs.knownAtcCallsigns():
			if env.ATCs.landLineStatus(atc) is not None:
				self.destroyLandLine(atc)
		self.wait()
		#py_audio.terminate() # CAUTION student may later reconnect

	def radioPTT(self):
		return self.radio_PTT

	def setRadioPTT(self, toggle):
		self.radio_PTT = toggle

	def receiveAudio(self, data):
		if self.running and not settings.radios_silenced:
			self.audio_out.write(data)

	## Defining AbstractLandLineManager methods below
	def startVoiceWith(self, atc):
		self.call_in_progress = atc

	def stopVoice(self):
		self.call_in_progress = None

	def sendLandLineRequest(self, atc):
		self.messageToSend.emit(TeachingMsg(TeachingMsg.PHONE_REQUEST, data=('1 %s' % atc)))

	def sendLandLineDrop(self, atc):
		self.messageToSend.emit(TeachingMsg(TeachingMsg.PHONE_REQUEST, data=('0 %s' % atc)))




# -------------------------------

class TeacherSessionManager(SessionManager):
	def __init__(self, gui):
		SessionManager.__init__(self, gui, SessionType.TEACHER)
		self.session_ticker = Ticker(gui, self.tickSessionOnce)
		self.simulation_paused = False
		self.acft_transmitting = None
		self.phone_radio_manager = None
		self.server = QTcpServer(gui)
		self.student_socket = None
		self.server.newConnection.connect(self.studentConnects)
		self.aircraft_list = [] # ControlledAiAircraft list
		self.noACK_traffic_count = 0
	
	def start(self):
		self.aircraft_list.clear()
		self.simulation_paused = False
		self.acft_transmitting = None
		self.session_ticker.startTicking(teacher_ticker_interval)
		self.server.listen(port=settings.teaching_service_port)
		if pyaudio_available and settings.phone_lines_enabled:
			self.phone_radio_manager = TeachingPhoneRadioManager(self.gui)
			self.phone_radio_manager.messageToSend.connect(self.phoneRadioMsgToSend)
		print('Teaching server ready on port %d' % settings.teaching_service_port)
		signals.specialTool.connect(self.createNewTraffic)
		signals.newATC.connect(self.sendATCs)
		signals.kbdPTT.connect(self.sendPTT)
		signals.sessionStarted.emit(SessionType.TEACHER)
	
	def stop(self):
		if self.isRunning():
			self.session_ticker.stop()
			if self.studentConnected():
				self.shutdownStudentConnection()
			if self.phone_radio_manager is not None:
				self.phone_radio_manager.messageToSend.disconnect(self.phoneRadioMsgToSend)
				self.phone_radio_manager = None
			signals.kbdPTT.disconnect(self.sendPTT)
			signals.newATC.disconnect(self.sendATCs)
			signals.specialTool.disconnect(self.createNewTraffic)
			self.server.close()
			self.aircraft_list.clear()
			self.simulation_paused = False
			self.acft_transmitting = None
			signals.sessionEnded.emit(SessionType.TEACHER)
	
	def isRunning(self):
		return self.session_ticker.isActive() or self.simulation_paused
	
	def pauseSession(self):
		self.simulation_paused = True
		self.session_ticker.stop()
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.SIM_PAUSED))
		signals.sessionPaused.emit()
	
	def resumeSession(self):
		self.simulation_paused = False
		self.session_ticker.startTicking(teacher_ticker_interval)
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.SIM_RESUMED))
		signals.sessionResumed.emit()

	def skipTimeForward(self, time_skipped):
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.TIME_SKIP, data=str(time_skipped.total_seconds())))
		acft_list = self.getAircraft()
		time_step = timedelta(milliseconds=teacher_ticker_interval)
		for i in range(int(time_skipped / time_step)):
			for acft in acft_list:
				acft.moveHistoryTimesForward(-time_step)
				acft.tickOnce()
		signals.realTimeSkipped.emit(time_skipped)
	
	def getAircraft(self):
		return self.aircraft_list[:]
	
	
	## ACFT/ATC INTERACTION
	
	def instructAircraftByCallsign(self, callsign, instr):
		print('INTERNAL ERROR: TeacherSessionManager.instructAircraftByCallsign called.', callsign, file=stderr)
	
	def postRadioChatMsg(self, msg):
		raise TextMsgBlocked('Radio chat panel in teacher sessions is reserved for monitoring read-backs. '
				'Use the ATC text chat system to communicate with the student.')
	
	def postAtcChatMsg(self, msg):
		if self.studentConnected():
			if msg.isPrivate():
				payload = '%s\n%s' % (msg.sender(), msg.txtOnly())
				self.student.sendMessage(TeachingMsg(TeachingMsg.ATC_TEXT_CHAT, data=payload))
			else:
				raise TextMsgBlocked('Only private messaging is enabled in tutoring sessions.')
		else:
			raise TextMsgBlocked('No student connected.')
	
	# NOTE with teacher: ATC arg is who sends the strip to student (not who receives it)
	def sendStrip(self, strip, atc):
		if self.studentConnected():
			msg_data = atc + '\n' + strip.encodeDetails(handover_details)
			self.student.sendMessage(TeachingMsg(TeachingMsg.STRIP_EXCHANGE, data=msg_data))
		else:
			raise HandoverBlocked('No student connected.')
	
	def sendCpdlcMsg(self, callsign, msg):
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.CPDLC, data=('%s\n%s%s' % (callsign, CPDLC_cmd_prefix_msg, msg.toEncodedStr()))))
		else:
			raise CpdlcOperationBlocked('No student connected.')
	
	# NOTE with teacher: ATC is who is proposing/cancelling the transfer
	def sendCpdlcTransferRequest(self, acft_callsign, atc_callsign, proposing):
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.CPDLC,
					data=('%s\n%s%d %s' % (acft_callsign, CPDLC_cmd_prefix_xfr, proposing, atc_callsign))))
		else:
			raise CpdlcOperationBlocked('No student connected.')
	
	# NOTE with teacher: ATC is who is accepting/rejecting the transfer
	def sendCpdlcTransferResponse(self, acft_callsign, atc_callsign, accept):
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.CPDLC, data=('%s\n%s%d' % (acft_callsign, CPDLC_cmd_prefix_xfr, accept))))
		else:
			raise CpdlcOperationBlocked('No student connected.')
	
	# NOTE with teacher: this is ACFT disconnecting itself
	def disconnectCpdlc(self, callsign):
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.CPDLC, data=('%s\n%s0' % (callsign, CPDLC_cmd_prefix_conn))))
		else:
			raise CpdlcOperationBlocked('No student connected.')
	
	
	## VOICE COMM'S
	
	def createRadio(self):
		QMessageBox.critical(self.gui, 'Create radio', 'No radio boxes in teaching sessions. '
				'Radio transmissions all happen on a single virtual frequency. Use keyboard PTT key to transmit. '
				'The signal source is the aircraft selected on PTT press, if any and if spawned (otherwise undetected).')
	
	def recordAtis(self, parent_dialog):
		pass

	def landLineManager(self):
		return self.phone_radio_manager # can be None
	
	
	## ONLINE SYSTEMS
	
	def weatherLookUpRequest(self, station):
		pass # weather never changes outside of teacher's action; no weather exists outside of primary station
	
	def pushFplOnline(self, fpl):
		if fpl.isOnline():
			fpl.modified_details.clear()
		else:
			used_IDs = {got.online_id for got in env.FPLs.findAll(pred=FPL.isOnline)}
			i = 0
			while '%s-%X' % (teacher_callsign, i) in used_IDs:
				i += 1
			fpl.markAsOnline('%s-%X' % (teacher_callsign, i))
		env.FPLs.refreshViews()
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.FLIGHT_PLAN, data=fpl.encode()))
	
	def changeFplStatus(self, fpl, new_status):
		fpl.setOnlineStatus(new_status)
		env.FPLs.refreshViews()
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.FLIGHT_PLAN, data=fpl.encode()))
	
	def syncOnlineFPLs(self):
		pass # teacher's online FPL list *is* the up-to-date online set
	
	
	## MANAGER-SPECIFIC
	
	def tickSessionOnce(self):
		for acft in pop_all(self.aircraft_list, lambda a: not env.pointInRadarRange(a.params.position)):
			self.killAircraft(acft) # this will send KILL to student
		send_traffic_this_tick = self.studentConnected() and self.noACK_traffic_count < max_noACK_traffic
		for acft in self.aircraft_list:
			acft.tickOnce()
			fgms_packet = acft.fgmsLivePositionPacket()
			send_packet_to_views(fgms_packet)
			if send_traffic_this_tick and acft.spawned:
				self.student.sendMessage(TeachingMsg(TeachingMsg.TRAFFIC, data=fgms_packet))
				self.noACK_traffic_count += 1
	
	
	# Teacher traffic/env. management
	
	def createNewTraffic(self, spawn_coords, spawn_hdg):
		dialog = CreateTrafficDialog(spawn_coords, spawn_hdg, parent=self.gui)
		dialog.exec()
		if dialog.result() > 0:
			params, status = dialog.acftInitParamsAndStatus()
			acft = ControlledAiAircraft(dialog.acftCallsign(), dialog.acftType(), params, status, None)
			acft.spawned = False
			acft.frozen = dialog.startFrozen()
			acft.tickOnce()
			self.aircraft_list.append(acft)
			if dialog.createStrip():
				strip = Strip()
				strip.writeDetail(FPL.CALLSIGN, acft.identifier)
				strip.writeDetail(FPL.ACFT_TYPE, acft.aircraft_type)
				strip.writeDetail(FPL.WTC, wake_turb_cat(acft.aircraft_type))
				strip.linkAircraft(acft)
				signals.receiveStrip.emit(strip)
			env.radar.scanSingleAcft(acft)
			selection.selectAircraft(acft)
	
	def killAircraft(self, acft):
		acft.endAllRdfUpdates()
		pop_all(self.aircraft_list, lambda a: a is acft) # NOTE: might already be removed from list (in tickSessionOnce)
		if acft.spawned and self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.ACFT_KILLED, data=acft.identifier))
	
	def setWeather(self, weather): # NOTE: argument weather should be from primary station
		register_weather_information(weather)
		self.sendPrimaryWeather()
	
	def requestCpdlcLogOn(self, callsign): # NOTE: student must confirm log-on
		if self.studentConnected():
			self.student.sendMessage(TeachingMsg(TeachingMsg.CPDLC, data=('%s\n%s1' % (callsign, CPDLC_cmd_prefix_conn))))
		else:
			raise CpdlcOperationBlocked('No student connected.')
	
	
	# Snapshotting
	
	def situationSnapshot(self):
		return [acft.statusSnapshot() for acft in self.aircraft_list]
	
	def restoreSituation(self, situation_snapshot):
		while self.aircraft_list:
			self.killAircraft(self.aircraft_list[0])
		for acft_snapshot in situation_snapshot:
			self.aircraft_list.append(ControlledAiAircraft.fromStatusSnapshot(acft_snapshot))
		self.tickSessionOnce()
	
	
	# Connection with student
	
	def studentConnected(self):
		return self.student_socket is not None
	
	def studentConnects(self):
		new_connection = self.server.nextPendingConnection()
		if new_connection:
			peer_address = new_connection.peerAddress().toString()
			print('Contacted by %s' % peer_address)
			if self.studentConnected():
				new_connection.disconnectFromHost()
				print('Client rejected. Student already connected.', file=stderr)
			else:
				self.student_socket = new_connection
				self.student_socket.disconnected.connect(self.studentDisconnects)
				self.student_socket.disconnected.connect(self.student_socket.deleteLater)
				self.student = TeachingSessionWire(self.student_socket)
				self.student.messageArrived.connect(self.receiveMsgFromStudent)
				env.ATCs.updateATC(student_callsign, None, None, None, insertAtTop=True)
				self.noACK_traffic_count = 0
				self.sendPrimaryWeather()
				self.sendATCs()
				for fpl in env.FPLs.findAll(pred=FPL.isOnline):
					self.student.sendMessage(TeachingMsg(TeachingMsg.FLIGHT_PLAN, data=fpl.encode()))
				if self.phone_radio_manager is not None:
					self.student.sendMessage(TeachingMsg(TeachingMsg.PHONE_REQUEST)) # with empty data = please answer if able
				self.tickSessionOnce()
				if self.simulation_paused:
					self.student.sendMessage(TeachingMsg(TeachingMsg.SIM_PAUSED))
				QMessageBox.information(self.gui, 'Student connection', 'Student accepted from %s' % peer_address)
		else:
			print('WARNING: Connection attempt failed.', file=stderr)
	
	def studentDisconnects(self):
		self.shutdownStudentConnection()
		QMessageBox.information(self.gui, 'Student disconnection', 'Your student has disconnected.')
	
	def shutdownStudentConnection(self):
		self.student_socket.disconnected.disconnect(self.studentDisconnects)
		if self.phone_radio_manager is not None:
			self.phone_radio_manager.stopAndWait()
		env.cpdlc.clearHistory()
		env.ATCs.removeATC(student_callsign)
		self.student.messageArrived.disconnect(self.receiveMsgFromStudent)
		self.student_socket.disconnectFromHost()
		self.student_socket = None
	
	def sendATCs(self):
		if self.studentConnected():
			msg = TeachingMsg(TeachingMsg.ATC_LIST)
			for atc in env.ATCs.knownAtcCallsigns(lambda a: a.callsign != student_callsign):
				try:
					frq = env.ATCs.getATC(atc).frequency # CommFrequency or None
				except KeyError:
					frq = None
				msg.appendData(atc if frq is None else '%s\t%s' % (atc, frq))
				msg.appendData('\n')
			self.student.sendMessage(msg)
	
	def sendPrimaryWeather(self):
		w = env.primaryWeather()
		if self.studentConnected() and w is not None:
			self.student.sendMessage(TeachingMsg(TeachingMsg.WEATHER, data=w.METAR()))

	def sendPTT(self, on_off):
		if self.acft_transmitting is not None:
			self.acft_transmitting.endRdfUpdates(self.acft_transmitting.identifier) # unique radio assumed
			if self.studentConnected():
				self.student.sendMessage(TeachingMsg(TeachingMsg.RADIO_PTT, data=('0 ' + self.acft_transmitting.identifier)))
			self.acft_transmitting = None
		if on_off and selection.acft is not None and selection.acft.spawned:
			self.acft_transmitting = selection.acft
			self.acft_transmitting.beginRdfUpdates(self.acft_transmitting.identifier) # unique radio assumed
			if self.studentConnected():
				self.student.sendMessage(TeachingMsg(TeachingMsg.RADIO_PTT, data=('1 ' + self.acft_transmitting.identifier)))

	def phoneRadioMsgToSend(self, msg):
		if self.studentConnected():
			self.student.sendMessage(msg)
	
	def receiveMsgFromStudent(self, msg):
		#DEBUG if msg.type != TeachingMsg.TRAFFIC:
		#DEBUG 	print('=== TEACHERS RECEIVES ===\n%s\n=== End ===' % msg.data)
		if msg.type == TeachingMsg.ATC_TEXT_CHAT:
			lines = msg.strData().split('\n')
			if len(lines) == 2:
				signals.incomingAtcTextMsg.emit(ChatMessage(student_callsign, lines[1], recipient=lines[0], private=True))
			else:
				print('ERROR: Invalid format in received ATC text chat from student.', file=stderr)
		
		elif msg.type == TeachingMsg.STRIP_EXCHANGE:
			line_sep = msg.strData().split('\n', maxsplit=1)
			toATC = line_sep[0]
			strip = Strip.fromEncodedDetails('' if len(line_sep) < 2 else line_sep[1])
			strip.writeDetail(received_from_detail, student_callsign)
			if toATC != teacher_callsign:
				strip.writeDetail(sent_to_detail, toATC)
			signals.receiveStrip.emit(strip)
		
		elif msg.type == TeachingMsg.FLIGHT_PLAN:
			fpl = FPL.fromEncoded(msg.strData())
			if fpl.isOnline():
				env.FPLs.updateFromOnlineDownload(fpl)
			else:
				print('ERROR: Received an offline FPL from student.', file=stderr)
		
		elif msg.type == TeachingMsg.WEATHER: # requesting weather information
			if msg.strData() == settings.primary_METAR_station:
				self.sendPrimaryWeather()
		
		elif msg.type == TeachingMsg.TRAFFIC: # acknowledging a traffic message
			if self.noACK_traffic_count > 0:
				self.noACK_traffic_count -= 1
			else:
				print('ERROR: Student acknowledging unsent traffic?!', file=stderr)
		
		elif msg.type == TeachingMsg.CPDLC:
			# Student msg format in 2 lines, first being ACFT callsign, second is either of the following:
			#  - student disconnects or rejects data link: CPDLC_cmd_prefix_conn + "0"
			#  - student accepts ACFT log-on: CPDLC_cmd_prefix_conn + "1"
			#  - student proposes or cancels transfer: CPDLC_cmd_prefix_xfr + "0"/"1" + space + ATC callsign
			#  - student accepts or rejects our transfer proposal: CPDLC_cmd_prefix_xfr + "0"/"1"
			#  - other CPDLC message: CPDLC_cmd_prefix_msg + encoded message string
			try:
				acft_callsign, line2 = msg.strData().split('\n', maxsplit=1)
				link = env.cpdlc.lastDataLink(acft_callsign)
				if line2 == CPDLC_cmd_prefix_conn + '0':
					if link is None or link.isTerminated(): # student is rejecting a log-on
						QMessageBox.warning(self.gui, 'CPDLC connection failed', 'Student is not accepting CPDLC connections.')
					else: # ACFT disconnected by student
						link.markProblem('Student disconnected aircraft')
						link.terminate(True)
				elif line2 == CPDLC_cmd_prefix_conn + '1': # ACFT log-on confirmed
					env.cpdlc.beginDataLink(acft_callsign)
				elif line2.startswith(CPDLC_cmd_prefix_xfr):
					positive = line2[len(CPDLC_cmd_prefix_xfr)] == '1' # IndexError is guarded here
					if ' ' in line2: # student proposing or cancelling XFR
						atc = line2.split(' ', maxsplit=1)[1]
						if positive: # student proposing XFR
							signals.cpdlcWindowRequest.emit(acft_callsign, False)
							if link is not None and link.isLive():
								accept = QMessageBox.question(self.gui, 'CPDLC transfer from student',
										'Accept data authority transfer for %s from student to %s?' % (acft_callsign, atc)) == QMessageBox.Yes
								if accept:
									link.setTransferTo(atc)
									link.terminate(True)
								try:
									self.sendCpdlcTransferResponse(acft_callsign, atc, accept)
								except CpdlcOperationBlocked as err:
									print('CPDLC error:', str(err), file=stderr)
							else:
								print('ERROR: Student proposing a transfer without data authority.', file=stderr)
						else:
							print('Student should not be able to abort a transfer.', file=stderr)
					else: # student accepting or rejecting XFR
						if link is None or link.pendingTransferFrom() is None:
							print('Ignored CPDLC transfer confirmed while none pending for %s.' % acft_callsign, file=stderr)
						elif positive:
							link.acceptIncomingTransfer()
						else: # student rejecting XFR
							link.markProblem('Student rejected transfer')
							link.terminate(False)
				elif line2.startswith(CPDLC_cmd_prefix_msg): # ACFT sending a message
					encoded_msg = line2[len(CPDLC_cmd_prefix_msg):]
					link = env.cpdlc.liveDataLink(acft_callsign)
					if link is None:
						print('Ignored CPDLC message sent from %s while not connected.' % acft_callsign, file=stderr)
					else:
						link.appendMessage(CpdlcMessage.fromEncodedStr(encoded_msg))
				else:
					print('Error decoding CPDLC command from student:', line2, file=stderr)
			except (IndexError, ValueError):
				print('Error decoding CPDLC message from student', file=stderr)

		elif msg.type == TeachingMsg.RADIO_AUDIO:
			if self.phone_radio_manager is not None and not self.phone_radio_manager.radioPTT():
				self.phone_radio_manager.receiveAudio(msg.binData())

		elif msg.type == TeachingMsg.PHONE_REQUEST:
			if self.phone_radio_manager is not None:
				tokens = msg.strData().split(maxsplit=1)
				if len(tokens) == 0: # student answering that audio is supported on their side
					self.phone_radio_manager.start()
				elif len(tokens) == 2 and tokens[0] == '0':
					self.phone_radio_manager.receiveLandLineDrop(tokens[1])
				elif len(tokens) == 2 and tokens[0] == '1':
					self.phone_radio_manager.receiveLandLineRequest(tokens[1])
				else:
					print('Error decoding phone request/drop message from student', file=stderr)

		elif msg.type == TeachingMsg.PHONE_AUDIO:
			if self.phone_radio_manager is not None:
				self.phone_radio_manager.receiveAudio(msg.binData())
